/**
 * @Author: p_starxxliu
 * @Date: 2021/4/21 1:45 下午
 */

package jwt_token

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/dgrijalva/jwt-go"
)

const (
	//ErrorReason_ServerBusy = "服务器繁忙"
	//ErrorReason_ReLogin    = "请重新登陆"

	EnErrorReason_ServerBusy = "server busy"
	EnErrorReason_ReLogin    = "please relogin"
	ClaimsKey                = "claims"
)

//JWTClaims token里面添加用户信息，验证token后可能会用到用户信息
type JWTClaims struct {
	jwt.StandardClaims
	UserID      int64    `json:"user_id"`
	Password    string   `json:"password"`
	Username    string   `json:"username"`
	FullName    string   `json:"full_name"`
	UserType    string   `json:"user_type"`
	Permissions []string `json:"permissions"`
}

func (claims JWTClaims) String() string {
	claimsByte, _ := json.Marshal(claims)
	return string(claimsByte)
}

func CreateClaims(data []byte) (*JWTClaims, error) {
	claims := &JWTClaims{}

	err := json.Unmarshal(data, claims)
	if err != nil {
		return nil, err
	}
	return claims, nil
}

var (
	Secret       = "middleware_weixin" // key
	ExpireTime   = 3600                // token有效期，如果使用white_ip 则不起作用
	IntervalTime = 600                 // 修改token的时间间隔
)

/*func login(c *gin.Context) {
	username := c.Param("username")
	password := c.Param("password")

	claims := &JWTClaims{
		UserID:      1,
		Username:    username,
		Password:    password,
		FullName:    username,
		Permissions: []string{},
	}

	claims.IssuedAt = time.Now().Unix()

	claims.ExpiresAt = time.Now().Add(time.Second * time.Duration(ExpireTime)).Unix()

	signedToken, err := GetToken(claims)
	if err != nil {
		c.String(http.StatusNotFound, err.Error())
		return
	}

	c.String(http.StatusOK, signedToken)
}*/

/*func verify(c *gin.Context) {
	strToken := c.Request.Header.Get("token")

	claim, _, err := VerifyAction(strToken)
	if err != nil {
		c.String(http.StatusNotFound, err.Error())
		return
	}

	c.String(http.StatusOK, "verify,", claim.Username)
}*/

/*func refresh(c *gin.Context) {
	strToken := c.Param("token")

	claims, _, err := VerifyAction(strToken)
	if err != nil {
		c.String(http.StatusNotFound, err.Error())
		return
	}

	claims.ExpiresAt = time.Now().Unix() + (claims.ExpiresAt - claims.IssuedAt)

	signedToken, err := GetToken(claims)
	if err != nil {
		c.String(http.StatusNotFound, err.Error())
		return
	}

	c.String(http.StatusOK, signedToken)
}*/

func VerifyAction(strToken string) (*JWTClaims, string, error) {
	token, err := jwt.ParseWithClaims(strToken, &JWTClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(Secret), nil
	})
	if err != nil {
		return nil, "", fmt.Errorf(EnErrorReason_ReLogin)
	}

	claims, ok := token.Claims.(*JWTClaims)
	if !ok {
		return nil, "", fmt.Errorf(EnErrorReason_ReLogin)
	}

	err = token.Claims.Valid()
	if err != nil {
		return nil, "", fmt.Errorf(EnErrorReason_ReLogin)
	}

	var signToken string
	if claims.ExpiresAt+(time.Second*time.Duration(ExpireTime)).Nanoseconds() < time.Now().Unix() {
		claims.ExpiresAt = time.Now().Add(time.Second * time.Duration(ExpireTime)).Unix()

		signToken, err = GetToken(claims)
		if err != nil {
			return nil, "", fmt.Errorf(EnErrorReason_ReLogin)
		}
	}

	return claims, signToken, nil
}

//GetToken create token info
func GetToken(claims *JWTClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	signedToken, err := token.SignedString([]byte(Secret))
	if err != nil {
		return "", fmt.Errorf(EnErrorReason_ServerBusy)
	}

	return signedToken, nil
}
