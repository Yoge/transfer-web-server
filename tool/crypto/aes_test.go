/**
 * @Author: p_starxxliu
 * @Date: 2021/4/17 2:28 下午
 */
package crypto

import (
	"encoding/hex"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestAesDecrypto(t *testing.T) {
	key := []byte("123456")                         //加密密钥
	keyHash, err := HashCompile(HASH_TYPE_SM3, key) //使用md5将key 变为16字节
	require.Nil(t, err)

	raw := []byte("123456") //加密原始数据
	res, err := AesEncrypto(keyHash, raw)
	println(hex.EncodeToString(res))
	require.Nil(t, err)

	answor, err := AesDecrypto(keyHash, res)
	require.Nil(t, err)
	require.Equal(t, raw, answor)
}
