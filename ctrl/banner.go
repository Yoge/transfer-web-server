package ctrl

import (
	"chainmaker.org/chainmaker/transfer-web-server/db/sqlite"
	"chainmaker.org/chainmaker/transfer-web-server/loggers"
	"fmt"
	"github.com/gin-gonic/gin"
)

var (
	chainType = []string{"fabric", "chainmaker"}
)

// GetBannerTasks godoc
// @Summary      显示迁移任务首页信息
// @Description  得到迁移任务概览信息
// @Tags         tasks
// @Accept       json
// @Produce      json
// @Success      200  {object}  ctrl.Response{result=ctrl.BannerRes}
// @Failure      400  {object}  ctrl.Response
// @Failure      500  {object}  ctrl.Response
// @Router       /tasks/banner [get]
func GetBannerTasks(c *gin.Context) {
	var (
		logger = loggers.GetLogger(loggers.MODULE_SDK)
		res    = &BannerRes{
			BannerChain: make([]*BannerChain, 0, 1),
		}
	)
	ti := &sqlite.TaskInfo{}
	db := sqlite.GetDB()

	total, err := ti.QueryTasksCount(db)
	if err != nil {
		WriteJSON(c, 500, fmt.Sprintf("query task fail,err [%s]", err), nil)
		return
	}
	AnomalyNum, err := ti.QueryTasksCountByStatus(db, 5)
	if err != nil {
		WriteJSON(c, 500, fmt.Sprintf("query task fail,err [%s]", err), nil)
		return
	}

	res.TaskNum = uint64(total)
	res.NormalNum = uint64(total - AnomalyNum)
	res.AnomalyNum = uint64(AnomalyNum)

	for _, name := range chainType {
		chain := &BannerChain{}
		tasks, err := ti.QueryTasksCountByChainType(db, name)
		if err != nil {
			logger.Error(err)
			WriteJSON(c, 500, fmt.Sprintf("query task fail,err [%s]", err), nil)
			return
		}
		if len(tasks) == 0 {
			continue
		}
		chain.OriginChainType = tasks[0].OriginChainType
		chain.OriginChainVersion = tasks[0].OriginChainVersion
		chain.TargetChainType = tasks[0].TargetChainType
		chain.TargetChainVersion = tasks[0].TargetChainVersion
		chain.TaskNum = uint64(len(tasks))

		for _, task := range tasks {
			if task.Status == 5 {
				chain.AnomalyNum++
			} else {
				chain.NormalNum++
			}
		}

		res.BannerChain = append(res.BannerChain, chain)
	}

	WriteJSON(c, 200, "", res)
}

// GetTasks godoc
// @Summary      查询任务
// @Description  根据条件查询任务信息，默认是查询全部信息
// @Tags         tasks
// @Accept       json
// @Produce      json
// @Param        task body      ctrl.TasksInfoReq  true  "查询任务参数"
// @Success      200  {object}  ctrl.Response{result=ctrl.TasksInfoRes}
// @Failure      400  {object}  ctrl.Response
// @Failure      500  {object}  ctrl.Response
// @Router       /tasks/query [post]
func GetTasks(c *gin.Context) {
	var (
		req    = &TasksInfoReq{}
		logger = loggers.GetLogger(loggers.MODULE_SDK)
		err    error
		res    = &TasksInfoRes{}
	)

	err = c.BindJSON(req)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	offsetIndex := req.PageLimit * (req.PageNum - 1)

	res.PageLimit = req.PageLimit
	res.PageNum = req.PageNum

	tasks := make([]*sqlite.TaskInfo, 0)

	//total count of matching taskName
	var count int64
	db := sqlite.GetDB()

	if req.TaskStatus == -1 {

		tasks, err = sqlite.QueryTasksPagingByName(offsetIndex, req.PageLimit, db, req.TaskName, req.TaskId)
		if err == nil {
			count, err = sqlite.QueryTasksPagingByNameCount(db, req.TaskName, req.TaskId)
		}
	} else {
		tasks, err = sqlite.QueryTasksPagingByNameAndStatusAndTaskId(offsetIndex, req.PageLimit, db, req.TaskName, req.TaskStatus, req.TaskId)
		if err == nil {
			count, err = sqlite.QueryTasksPagingByNameAndStatusAndTaskIdCount(db, req.TaskName, req.TaskStatus, req.TaskId)
		}
	}

	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), &BannerRes{})
	}
	res.Tasks = tasks
	res.TaskTotal = count
	WriteJSON(c, 200, "", res)
}
