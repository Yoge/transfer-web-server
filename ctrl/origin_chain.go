package ctrl

import (
	"fmt"
	"os"
	"strings"

	cmcommon "chainmaker.org/chainmaker-go/pb/protogo/common"
	"chainmaker.org/chainmaker/transfer-tool/core"
	"chainmaker.org/chainmaker/transfer-web-server/db/sqlite"
	"chainmaker.org/chainmaker/transfer-web-server/loggers"
	"chainmaker.org/chainmaker/transfer-web-server/transfer_tool"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

var (
	SavePath   = "./config_path/"
	OriginPath = "origin"
	TargetPath = "target"
	DataPath   = "data"
)

// AddOriginChain godoc
// @Summary      添加原链信息
// @Description  添加原链信息，包括zip压缩的证书，与fabric sdk文件
// @Tags         AddTask
// @Accept       multipart/form-data
// @Produce      json
// @Param        task_id       query     string  true  "任务id"
// @Param        channel_name  query     string  true  "原链通道name"
// @Param        user_name     query     string  true  "原链用户名，必须与sdk相匹配"
// @Param        file_certs    formData  file    true  "原链证书"
// @Param        file_sdk      formData  file    true  "原链sdk配置文件"
// @Success      200     {object}  ctrl.Response
// @Failure      400     {object}  ctrl.Response
// @Failure      404     {object}  ctrl.Response
// @Failure      500     {object}  ctrl.Response
// @Router       /create_task/add_origin_chain [post]
func AddOriginChain(c *gin.Context) {
	var (
		logger = loggers.GetLogger(loggers.MODULE_SDK)
		err    error
		res    = &sqlite.OriginChainInfo{}
	)

	req, err := checkOriginChainParam(c, logger)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	res.TaskId = req.TaskId
	res.ChannelName = req.ChannelName
	res.UserName = req.UserName

	sdkfile, err := c.FormFile("file_sdk")
	if err != nil {
		err = fmt.Errorf("sdk file failed,err: %s", err)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}
	if !(strings.HasSuffix(sdkfile.Filename, ".yaml") || strings.HasSuffix(sdkfile.Filename, ".yml")) {
		err = fmt.Errorf("file_sdk must is yaml config file")
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}
	certsfile, err := c.FormFile("file_certs")
	if err != nil {
		err = fmt.Errorf("sdk file failed,err: %s", err)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}
	if !strings.HasSuffix(certsfile.Filename, ".zip") {
		err = fmt.Errorf("file_certs must is yaml config file")
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	pathname := SavePath + res.TaskId + string(os.PathSeparator) + OriginPath + string(os.PathSeparator)
	if _, err := os.Stat(pathname); err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(pathname, 0777)
			if err != nil {
				logger.Error(err)
				WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
				return
			}
		}
	}

	res.SdkPath = pathname + sdkfile.Filename
	res.CertPath = pathname + certsfile.Filename
	err = c.SaveUploadedFile(sdkfile, res.SdkPath) //存储文件，设计的目标路径，./config_path/task_id/origin
	if err != nil {
		err = fmt.Errorf("save sdk file failed,err: %s", err)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}
	err = c.SaveUploadedFile(certsfile, res.CertPath) //存储文件，设计的目标路径，./config_path/task_id
	if err != nil {
		err = fmt.Errorf("save certs file failed,err: %s", err)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	err = DeCompressByZip(res.CertPath, pathname)
	if err != nil {
		err = fmt.Errorf("DeCompressByZip failed,err: %s", err)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	originChainInfo, err := sqlite.GetOriginChainInfoByTaskId(sqlite.GetDB(), res.TaskId)
	if err == nil {
		res.Id = originChainInfo.Id
		err = res.UpdateOriginChainInfo(sqlite.GetDB())
		if err != nil {
			logger.Error(err)
			WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
			return
		}
	} else {
		if err.Error() == "record not found" {
			_, err = res.InsertOriginChainInfo(sqlite.GetDB())
			if err != nil {
				logger.Error(err)
				WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
				return
			}
		} else {
			err = fmt.Errorf("get origin chain config failed,err: %s", err)
			logger.Error(err)
			WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
			return
		}
	}

	db := sqlite.GetDB()

	taskInfo, err := sqlite.GetTaskInfoByTaskId(db, res.TaskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	fetcher, err := core.GetOriginChainFetch(taskInfo.OriginChainType, taskInfo.OriginChainVersion, res.ChannelName, res.SdkPath, res.UserName)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}
	defer func() {
		fetcher.CloseSdk()
	}()

	latestheight, err := fetcher.GetLastBlockHeight()
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	taskInfo.LastHeight = latestheight

	err = taskInfo.UpdateChain(db)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	if taskInfo.TransferHeight > latestheight-1 {
		WriteJSON(c, 500, ErrCodeSystemInner.String(fmt.Errorf("TransferHeight is over origin chain lastest height, Please modify your transfer height not over fabric latest BlockHeight: %d", latestheight-1)), nil)
		return
	}

	WriteJSON(c, 200, "", "origin chain information save success! ")
}

// TestOriginChainSdk godoc
// @Summary      测试是否可以链接原链节点
// @Description  测试是否可以链接原链节点
// @Tags         tasks
// @Accept       json
// @Produce      json
// @Param        task_id       query     string  true  "任务id"
// @Success      200     {object}  ctrl.Response
// @Failure      400     {object}  ctrl.Response
// @Failure      404     {object}  ctrl.Response
// @Failure      500     {object}  ctrl.Response
// @Router       /test_origin_chain/{task_id} [get]
func TestOriginChainSdk(c *gin.Context) {
	var (
		logger = loggers.GetLogger(loggers.MODULE_SDK)
		err    error
		taskId = ""
		ok     bool
	)
	taskId, ok = c.GetQuery("task_id")
	if !ok {
		err = fmt.Errorf("channel_name can not is nil")
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	db := sqlite.GetDB()

	taskInfo, err := sqlite.GetTaskInfoByTaskId(db, taskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	originChainInfo, err := sqlite.GetOriginChainInfoByTaskId(db, taskId)
	if err != nil {
		err = fmt.Errorf("get origin chain config failed,err: %s", err)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	fetcher, err := core.GetOriginChainFetch(taskInfo.OriginChainType, taskInfo.OriginChainVersion, originChainInfo.ChannelName, originChainInfo.SdkPath, originChainInfo.UserName)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}
	defer func() {
		fetcher.CloseSdk()
	}()

	latestheight, err := fetcher.GetLastBlockHeight()
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	if taskInfo.TransferHeight > latestheight-1 {
		WriteJSON(c, 500, ErrCodeSystemInner.String(fmt.Errorf("TransferHeight is over origin chain lastest height, Please modify your transfer height not over fabric latest BlockHeight: %d", latestheight-1)), nil)
		return
	}

	block, err := fetcher.QueryBlockByHeight(2)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	if taskInfo.OriginChainType == "fabric" {
		WriteJSON(c, 200, "", gin.H{"Response": "Origin chain test success!!!", "latest fabric BlockHeight": latestheight - 1})
	} else if taskInfo.OriginChainType == "chainmaker" {
		WriteJSON(c, 200, "", gin.H{"Response": "Origin chain test success!!!", "BlockHeight": block.(*cmcommon.BlockInfo).Block.Header.BlockHeight})
	}
}

// UpdateOriginChain godoc
// @Summary      修改原链信息
// @Description  修改原链信息，包括zip压缩的证书，与fabric sdk文件
// @Tags         AddTask
// @Accept       multipart/form-data
// @Produce      json
// @Param        task_id       query     string  true  "任务id"
// @Param        channel_name  query     string  true  "原链通道name"
// @Param        user_name     query     string  true  "原链用户名，必须与sdk相匹配"
// @Param        file_certs    formData  file    false  "原链证书"
// @Param        file_sdk      formData  file    false  "原链sdk配置文件"
// @Success      200     {object}  ctrl.Response
// @Failure      400     {object}  ctrl.Response
// @Failure      404     {object}  ctrl.Response
// @Failure      500     {object}  ctrl.Response
// @Router       /create_task/update_origin_chain [post]
func UpdateOriginChain(c *gin.Context) {
	var (
		logger = loggers.GetLogger(loggers.MODULE_SDK)
		err    error
		res    = &sqlite.OriginChainInfo{}
	)
	req, err := checkOriginChainParam(c, logger)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	transfer, _ := transfer_tool.NewTransferTools(logger).GetTransfer(req.TaskId)
	if transfer != nil {
		err = fmt.Errorf("can not update task[%s] config. it is runing ", req.TaskId)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	res, err = sqlite.GetOriginChainInfoByTaskId(sqlite.GetDB(), req.TaskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	sdkfile, err := c.FormFile("file_sdk")
	if err != nil && err.Error() != "no multipart boundary param in Content-Type" && err.Error() != "http: no such file" {
		err = fmt.Errorf("read sdk file failed,err: %s", err)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}
	if sdkfile != nil {
		if !(strings.HasSuffix(sdkfile.Filename, ".yaml") || strings.HasSuffix(sdkfile.Filename, ".yml")) {
			err = fmt.Errorf("file_sdk must is yaml config file")
			logger.Error(err)
			WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
			return
		}

		err = os.RemoveAll(res.SdkPath)
		logger.Error(err)

		newSdkPath := SavePath + res.TaskId + string(os.PathSeparator) + OriginPath + string(os.PathSeparator) + sdkfile.Filename
		err = c.SaveUploadedFile(sdkfile, newSdkPath) //存储文件，设计的目标路径，./config_path/task_id
		if err != nil {
			err = fmt.Errorf("save sdk file failed,err: %s", err)
			logger.Error(err)
			WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
			return
		}

		//isChange := strings.HasSuffix(res.SdkPath, sdkfile.Filename)
		//if !isChange {
		//	err = os.RemoveAll(res.SdkPath)
		//	logger.Error(err)
		//}

		res.SdkPath = newSdkPath
	}

	certsfile, err := c.FormFile("file_certs")
	if err != nil && err.Error() != "no multipart boundary param in Content-Type" && err.Error() != "http: no such file" {
		err = fmt.Errorf("save cert file failed,err: %s", err)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	if certsfile != nil {
		if !strings.HasSuffix(certsfile.Filename, ".zip") {
			err = fmt.Errorf("file_certs must is yaml config file")
			logger.Error(err)
			WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
			return
		}

		err = os.RemoveAll(res.CertPath)
		logger.Error(err)

		newCertPath := SavePath + res.TaskId + string(os.PathSeparator) + OriginPath + string(os.PathSeparator) + certsfile.Filename

		err = c.SaveUploadedFile(certsfile, newCertPath) //存储文件，设计的目标路径，./config_path/task_id
		if err != nil {
			err = fmt.Errorf("save certs file failed,err: %s", err)
			logger.Error(err)
			WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
			return
		}
		//TODO 原子修改，解压文件的删除
		err = DeCompressByZip(res.CertPath, SavePath+res.TaskId+string(os.PathSeparator)+OriginPath+string(os.PathSeparator))
		if err != nil {
			err = fmt.Errorf("DeCompressByZip failed,err: %s", err)
			logger.Error(err)
			WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
			return
		}

		//isSuffix := strings.HasSuffix(res.CertPath, certsfile.Filename)
		//if !isSuffix {
		//	err = os.RemoveAll(res.CertPath)
		//	logger.Error(err)
		//}

		res.CertPath = newCertPath
	}

	res.ChannelName = req.ChannelName
	res.UserName = req.UserName

	err = res.UpdateOriginChainInfo(sqlite.GetDB())
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	db := sqlite.GetDB()

	taskInfo, err := sqlite.GetTaskInfoByTaskId(db, res.TaskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	fetcher, err := core.GetOriginChainFetch(taskInfo.OriginChainType, taskInfo.OriginChainVersion, res.ChannelName, res.SdkPath, res.UserName)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}
	defer func() {
		fetcher.CloseSdk()
	}()

	latestheight, err := fetcher.GetLastBlockHeight()
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	if taskInfo.TransferHeight > latestheight-1 {
		WriteJSON(c, 500, ErrCodeSystemInner.String(fmt.Errorf("TransferHeight is over origin chain lastest height, Please modify your transfer height not over fabric latest BlockHeight: %d", latestheight-1)), nil)
		return
	}
	taskInfo.LastHeight = latestheight

	err = taskInfo.UpdateChain(db)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	WriteJSON(c, 200, "", "origin chain information save success! ")
}

func checkOriginChainParam(c *gin.Context, logger *zap.SugaredLogger) (*OriginReq, error) {
	var err error
	res := &OriginReq{}
	var ok bool

	res.TaskId, ok = c.GetQuery("task_id")
	if !ok {
		err = fmt.Errorf("task_id can not is nil")
		logger.Error(err)
		return nil, err
	}

	res.ChannelName, ok = c.GetQuery("channel_name")
	if !ok {
		err = fmt.Errorf("channel_name can not is nil")
		logger.Error(err)
		return nil, err
	}
	res.UserName, ok = c.GetQuery("user_name")
	if !ok {
		err = fmt.Errorf("user_name can not is nil")
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return nil, err
	}

	return res, nil
}
