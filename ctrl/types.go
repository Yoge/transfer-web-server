package ctrl

import "chainmaker.org/chainmaker/transfer-web-server/db/sqlite"

type BannerRes struct {
	TaskNum     uint64 `json:"task_num"`    //任务总数
	NormalNum   uint64 `json:"normal_num"`  //正常任务总数
	AnomalyNum  uint64 `json:"anomaly_num"` //异常任务总数
	BannerChain []*BannerChain
}

type BannerChain struct {
	OriginChainType    string `json:"origin_chain_type"`    //原链类型
	OriginChainVersion string `json:"origin_chain_version"` //原链版本
	TargetChainType    string `json:"target_chain_type"`    //目标链类型
	TargetChainVersion string `json:"target_chain_version"` //目标链版本
	TaskNum            uint64 `json:"task_num"`             //任务总数
	NormalNum          uint64 `json:"normal_num"`           //正常任务数
	AnomalyNum         uint64 `json:"anomaly_num"`          //异常任务数
}

type TasksInfoReq struct {
	TaskId     string `json:"task_id"`      //根据任务Id查询任务
	TaskName   string `json:"task_name"`    //查询任务名，支持模糊查询
	TaskStatus int    `json:"task_status" ` //-1 表示全部状态
	PageNum    int    `json:"page_num"`     //当前页码
	PageLimit  int    `json:"page_limit"`   //期望的每页任务数
}

type TasksInfoRes struct {
	TaskTotal int64              `json:"task_total"` //任务总数
	PageNum   int                `json:"page_num"`   //当前页码
	PageLimit int                `json:"page_index"` //每页任务数
	Tasks     []*sqlite.TaskInfo `json:"tasks"`      //任务列表
}

type TaskReq struct {
	TaskName           string `json:"task_name" binding:"required"`            //任务name
	OriginChainType    string `json:"origin_chain_type" binding:"required"`    //原链类型
	OriginChainVersion string `json:"origin_chain_version" binding:"required"` //原链版本
	TargetChainType    string `json:"target_chain_type" binding:"required"`    //目标链类型
	TargetChainVersion string `json:"target_chain_version" binding:"required"` //目标链版本
	TransferHeight     uint64 `json:"transfer_height" `                        //迁移高度
}

type UpdateTaskReq struct {
	TaskId             string `json:"task_id" binding:"required"`              //任务ID
	TaskName           string `json:"task_name" binding:"required"`            //任务名
	OriginChainType    string `json:"origin_chain_type" binding:"required"`    //原链类型
	OriginChainVersion string `json:"origin_chain_version" binding:"required"` //原链版本
	TargetChainType    string `json:"target_chain_type" binding:"required"`    //目标链类型
	TargetChainVersion string `json:"target_chain_version" binding:"required"` //目标链版本
	TransferHeight     uint64 `json:"transfer_height" `                        //迁移高度
}

type CreateTaskRes struct {
	TaskId string `json:"task_id"`
}

type TaskInfo struct {
	LastHeight       uint64           `json:"last_height"`        //如果设置的迁移高度是0，这里则是原链最新高度，否则是迁移高度
	AlreadyHeight    uint64           `json:"already_height"`     //已经迁移高度
	Task             *sqlite.TaskInfo `json:"task"`               //任务信息
	OriginSdkPath    string           `json:"origin_sdk_path"`    //原链config文件路径
	OriginCertPath   string           `json:"origin_cert_path"`   //原链用户证书路径
	ChannelName      string           `json:"channel_name"`       //原链通道名
	UserName         string           `json:"user_name"`          //原链用户名
	AuthType         string           `json:"key_type"`           //目标链证书类型
	OrgId            string           `json:"org_id"`             //目标链组织
	TargetConfigPath string           `json:"target_config_path"` //目标链文件路径
}

type TaskLog struct {
	LastHeight    uint64           `json:"last_height"`    //如果设置的迁移高度是0，这里则是原链最新高度，否则是迁移高度
	AlreadyHeight uint64           `json:"already_height"` //已经迁移高度
	Task          *sqlite.TaskInfo `json:"task"`           //任务信息
}

type TargetReq struct {
	AuthType string `json:"auth_type"` //证书类型
	TaskId   string `json:"task_id"`   //任务Id
	OrgId    string `json:"org_id"`    //组织Id
}

type OriginReq struct {
	ChannelName string `json:"channel_name"` //原链通道name
	TaskId      string `json:"task_id"`      //任务Id
	UserName    string `json:"user_name"`    //原链用户name
}
