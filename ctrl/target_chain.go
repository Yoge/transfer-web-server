package ctrl

import (
	"chainmaker.org/chainmaker/transfer-web-server/common"
	"chainmaker.org/chainmaker/transfer-web-server/config/transfer_config"
	"chainmaker.org/chainmaker/transfer-web-server/db/sqlite"
	"chainmaker.org/chainmaker/transfer-web-server/loggers"
	"chainmaker.org/chainmaker/transfer-web-server/transfer_tool"
	"fmt"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
	"os"
	"strings"
)

// AddTargetChain godoc
// @Summary      添加目标链信息
// @Description  添加目标链信息，包括zip压缩的节点依赖文件
// @Tags         AddTask
// @Accept       multipart/form-data
// @Produce      json
// @Param        task_id       query     string  true  "任务id"
// @Param        auth_type  query     string  true  "证书类型"
// @Param        org_id    query     string  true  "节点id或者组织id"
// @Param        file    formData  file    true  "目标链配置信息"
// @Success      200     {object}  ctrl.Response
// @Failure      400     {object}  ctrl.Response
// @Failure      404     {object}  ctrl.Response
// @Failure      500     {object}  ctrl.Response
// @Router       /create_task/add_target_chain [post]
func AddTargetChain(c *gin.Context) {
	var (
		logger = loggers.GetLogger(loggers.MODULE_SDK)
		err    error
		res    = &sqlite.TargetChainInfo{}
	)

	req, err := checkTargetChainParam(c, logger)
	if err != nil {
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	res.TaskId = req.TaskId
	res.OrgId = req.OrgId
	res.AuthType = req.AuthType

	task, err := sqlite.GetTaskInfoByTaskId(sqlite.GetDB(), res.TaskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	if task.Status != 0 {
		err = fmt.Errorf("task[%s] has been configured ", res.TaskId)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	configFile, err := c.FormFile("file")
	if !strings.HasSuffix(configFile.Filename, ".zip") {
		err = fmt.Errorf("file_certs must is yaml config file")
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	pathName := SavePath + res.TaskId + string(os.PathSeparator) + TargetPath + string(os.PathSeparator)
	if _, err := os.Stat(pathName); err != nil {
		if os.IsNotExist(err) {
			err = os.MkdirAll(pathName, 0777)
			if err != nil {
				logger.Error(err)
				WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
				return
			}
		}
	}

	res.ConfigPath = pathName + configFile.Filename
	err = c.SaveUploadedFile(configFile, res.ConfigPath) //存储文件，设计的目标路径，./config_path/task_id
	if err != nil {
		err = fmt.Errorf("save config file failed,err: %s", err)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	err = DeCompressByZip(res.ConfigPath, pathName)
	if err != nil {
		err = fmt.Errorf("DeCompressByZip failed,err: %s", err)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		err = os.Remove(res.ConfigPath)
		logger.Error(err)
		return
	}

	err = transfer_config.CheckGenesisFile(res.OrgId, res.TaskId, res)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		err = os.Remove(res.ConfigPath)
		logger.Error(err)
		err = os.RemoveAll(SavePath + res.TaskId + string(os.PathSeparator) + res.OrgId)
		logger.Error(err)
		return
	}

	_, err = res.InsertTargetChainInfo(sqlite.GetDB())
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		err = os.Remove(res.ConfigPath)
		logger.Error(err)
		err = os.RemoveAll(SavePath + res.TaskId + string(os.PathSeparator) + res.OrgId)
		logger.Error(err)
		return
	}

	task.Status = 1
	task.UpdateChain(sqlite.GetDB())
	WriteJSON(c, 200, "", "target chain information save success! ")

}

// DownloadFile     godoc
// @Summary         下载文件
// @Description     下载证书或者配置文件
// @Tags file
// @Param        filename query string true "file name"
// @Success      200   {object}  ctrl.Response
// @Failure      400   {object}  ctrl.Response
// @Failure      404   {object}  ctrl.Response
// @Failure      500   {object}  ctrl.Response
// @Router /download [get]
func DownloadFile(c *gin.Context) {
	var (
		logger   = loggers.GetLogger(loggers.MODULE_SDK)
		err      error
		filename = ""
		ok       bool
	)
	filename, ok = c.GetQuery("filename")
	if !ok {
		err = fmt.Errorf("channel_name can not is nil")
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	fileinfo, err := os.Stat(filename)
	if err != nil {
		if os.IsNotExist(err) {
			err = fmt.Errorf("filename is not exist")
			logger.Error(err)
			WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
			return
		}
	}

	c.Writer.Header().Add("Content-Disposition", fmt.Sprintf("attachment; filename=%s", fileinfo.Name())) //fmt.Sprintf("attachment; filename=%s", filename)对下载的文件重命名
	c.Writer.Header().Add("Content-Type", "application/octet-stream")
	c.File(filename)

}

/*

 */

// UpdateTargetChain godoc
// @Summary      修改目标链信息
// @Description  修改目标链信息，包括zip压缩的节点依赖文件
// @Tags         AddTask
// @Accept       multipart/form-data
// @Produce      json
// @Param        task_id    query     string  true  "任务id"
// @Param        auth_type  query     string  true  "证书类型"
// @Param        org_id     query     string  true  "节点id或者组织id"
// @Param        file       formData  file    false  "目标链配置信息"
// @Success      200     {object}  ctrl.Response
// @Failure      400     {object}  ctrl.Response
// @Failure      404     {object}  ctrl.Response
// @Failure      500     {object}  ctrl.Response
// @Router       /create_task/update_target_chain [post]
func UpdateTargetChain(c *gin.Context) {
	var (
		logger = loggers.GetLogger(loggers.MODULE_SDK)
		err    error
		res    = &sqlite.TargetChainInfo{}
	)

	configFile, err := c.FormFile("file")
	if err != nil && err.Error() != "no multipart boundary param in Content-Type" && err.Error() != "http: no such file" {
		err = fmt.Errorf("read target chain config file failed,err: %s", err)
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), "read target chain config file err")
		return
	}

	req, err := checkTargetChainParam(c, logger)
	if err != nil {
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	transder, _ := transfer_tool.NewTransferTools(logger).GetTransfer(req.TaskId)
	if transder != nil {
		err = fmt.Errorf("can not update task[%s] config. it is runing ", req.TaskId)
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	//query target chain info from db
	res, err = sqlite.GetTargetChainInfoByTaskId(sqlite.GetDB(), req.TaskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	res.TaskId = req.TaskId
	res.OrgId = req.OrgId
	res.AuthType = req.AuthType

	task, err := sqlite.GetTaskInfoByTaskId(sqlite.GetDB(), res.TaskId)
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
		return
	}

	status := task.Status

	task.Status = 0
	err = task.UpdateChain(sqlite.GetDB())
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		return
	}

	if status <= 1 { //小于等于1 表示任务还未启动过，可以直接修改对应的配置

	} else { //否则我们需要del data文件
		err = os.RemoveAll(SavePath + res.TaskId + string(os.PathSeparator) + DataPath)
		logger.Error(err)
		task.AlreadyHeight = 0
	}

	if configFile != nil {
		if !strings.HasSuffix(configFile.Filename, ".zip") {
			err = fmt.Errorf("file_certs must is yaml config file")
			logger.Error(err)
			WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
			return
		}

		res.ConfigPath = SavePath + res.TaskId + string(os.PathSeparator) + TargetPath + string(os.PathSeparator) + configFile.Filename
		err = c.SaveUploadedFile(configFile, res.ConfigPath) //存储文件，设计的目标路径，./config_path/task_id
		if err != nil {
			err = fmt.Errorf("save config file failed,err: %s", err)
			logger.Error(err)
			WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
			return
		}

		err = DeCompressByZip(res.ConfigPath, SavePath+res.TaskId+string(os.PathSeparator)+TargetPath+string(os.PathSeparator))
		if err != nil {
			err = fmt.Errorf("DeCompressByZip failed,err: %s", err)
			logger.Error(err)
			WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
			err = os.Remove(res.ConfigPath)
			logger.Error(err)
			return
		}

		err = transfer_config.CheckGenesisFile(res.OrgId, res.TaskId, res)
		if err != nil {
			logger.Error(err)
			WriteJSON(c, 400, ErrCodeParamInvalid.String(err), nil)
			err = os.Remove(res.ConfigPath)
			logger.Error(err)
			err = os.RemoveAll(SavePath + res.TaskId + string(os.PathSeparator) + res.OrgId)
			logger.Error(err)
			return
		}
	}

	err = res.UpdateChain(sqlite.GetDB())
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		err = os.Remove(res.ConfigPath)
		logger.Error(err)
		err = os.RemoveAll(SavePath + res.TaskId + string(os.PathSeparator) + res.OrgId)
		logger.Error(err)
		return
	}

	task.Status = 1
	err = task.UpdateChain(sqlite.GetDB())
	if err != nil {
		logger.Error(err)
		WriteJSON(c, 500, ErrCodeSystemInner.String(err), nil)
		err = os.Remove(res.ConfigPath)
		logger.Error(err)
		err = os.RemoveAll(SavePath + res.TaskId + string(os.PathSeparator) + res.OrgId)
		logger.Error(err)
		return
	}

	WriteJSON(c, 200, "", "target chain information save success! ")
}

func checkTargetChainParam(c *gin.Context, logger *zap.SugaredLogger) (*TargetReq, error) {
	var err error
	res := &TargetReq{}
	var ok bool

	res.TaskId, ok = c.GetQuery("task_id")
	if !ok {
		err = fmt.Errorf("channel_name can not is nil")
		logger.Error(err)
		return nil, err
	}

	res.AuthType, ok = c.GetQuery("auth_type")
	if !ok {
		err = fmt.Errorf("channel_name can not is nil")
		logger.Error(err)
		return nil, err

	}

	if common.AuthTypeName[res.AuthType] == common.PermissionedWithCert || common.AuthTypeName[res.AuthType] == common.PermissionedWithKey {
		res.OrgId, ok = c.GetQuery("org_id")
		if !ok {
			err = fmt.Errorf("user_name can not is nil")
			logger.Error(err)
			return nil, err

		}
	} else if common.AuthTypeName[res.AuthType] == common.Public {
		res.OrgId = "node1"
	} else {
		err = fmt.Errorf("auth_type must is permissionedWithCert or public ")
		logger.Error(err)
		return nil, err

	}
	res.AuthType = strings.ToLower(res.AuthType)

	return res, nil
}
