package transfer_config

import (
	"fmt"
	"os"
	"strings"

	"path/filepath"

	"chainmaker.org/chainmaker/transfer-tool/common/chainconf"
	"chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-web-server/db/sqlite"
	"gopkg.in/yaml.v2"
)

var (
	chainMakerPath = "./config_path/%s/target/%s/chainmaker.yml"
	genesisPath    = "./config_path/%s/target/%s/chainconfig/bc1.yml"
	configPath     = "./config_path/%s/config-tool.yml"

	pubUserSignKeyFilePath  = "./config_path/%s/target/%s/user/client/client.key"
	pubPrivKeyFile          = "./config_path/%s/target/%s/node1.key"
	pubAdminSignKeyFilePath = "./config_path/%s/target/%s/admin/admin1/admin1.key"

	keyUserSignKeyFilePath  = "./config_path/%s/target/%s/keys/user/client1/client1.key"
	keyPrivKeyFile          = "./config_path/%s/target/%s/keys/node/consensus1/consensus1.key"
	keyAdminSignKeyFilePath = "./config_path/%s/target/%s/keys/admin/admin1/admin1.key"

	certUserSignKeyFilePath  = "./config_path/%s/target/%s/certs/user/client1/client1.sign.key"
	certUserSignCrtFilePath  = "./config_path/%s/target/%s/certs/user/client1/client1.sign.crt"
	certPrivKeyFile          = "./config_path/%s/target/%s/certs/node/consensus1/consensus1.sign.key"
	certCertFile             = "./config_path/%s/target/%s/certs/node/consensus1/consensus1.sign.crt"
	certAdminSignKeyFilePath = "./config_path/%s/target/%s/certs/user/admin1/admin1.sign.key"
	certAdminSignCrtFilePath = "./config_path/%s/target/%s/certs/user/admin1/admin1.sign.crt"
)

type CMConfig struct {
	StorageConfig map[string]interface{} `mapstructure:"storage" yaml:"storage"`
}

type Config struct {
	LogConfig    config.LogConfig       `mapstructure:"log" yaml:"log"`
	Chain        config.ChainClient     `mapstructure:"chain"`
	ContractInfo config.ContractInfo    `mapstructure:"contract_info" yaml:"contract_info"`
	OriginChain  config.OriginChain     `mapstructure:"origin_chain" yaml:"origin_chain"`
	Node         []*config.NodeInfo     `mapstructure:"node"`
	Storage      map[string]interface{} `yaml:"storage" mapstructure:"storage"`
	Async        *config.Async          `mapstructure:"async"`
}

func CreateConfig(taskId string) (string, *Config, error) {
	tConfig := &Config{}

	getDefaultLog(tConfig)
	orgId, err := CreatChainConfig(taskId, tConfig)
	if err != nil {
		return "", nil, err
	}

	err = CreateOriginChainConfig(taskId, tConfig)
	if err != nil {
		return "", nil, err
	}

	err = GetStorage(orgId, taskId, tConfig)
	if err != nil {
		return "", nil, err
	}

	err = CreateContract(tConfig)
	if err != nil {
		return "", nil, err
	}

	getDefaultAsync(tConfig)

	configB, err := yaml.Marshal(tConfig)
	if err != nil {
		return "", nil, err
	}

	path := fmt.Sprintf(configPath, taskId)

	err = os.WriteFile(path, configB, 0777)
	if err != nil {
		return "", nil, err
	}

	return path, tConfig, nil
}

func CreatChainConfig(taskId string, tConfig *Config) (orgId string, err error) {
	targetChain, err := sqlite.GetTargetChainInfoByTaskId(sqlite.GetDB(), taskId)
	if err != nil {
		return
	}

	orgId = targetChain.OrgId
	tConfig.Chain.ChainId = targetChain.ChainId
	tConfig.Chain.HashType = targetChain.HashAlgo
	tConfig.Chain.OrgId = targetChain.OrgId
	tConfig.Chain.AuthType = targetChain.AuthType
	tConfig.Chain.Genesis = targetChain.GenesisPath
	tConfig.Chain.ConsensusId = targetChain.ConsensusNodeId

	var admin *config.AdminUser
	amins := make([]*config.AdminUser, 0)

	switch tConfig.Chain.AuthType {
	case "permissionedwithcert":
		tConfig.Chain.UserSignKeyFilePath = fmt.Sprintf(certUserSignKeyFilePath, taskId, orgId)
		tConfig.Chain.UserSignCrtFilePath = fmt.Sprintf(certUserSignCrtFilePath, taskId, orgId)
		tConfig.Chain.PrivKeyFilePath = fmt.Sprintf(certPrivKeyFile, taskId, orgId)
		tConfig.Chain.CertFilePath = fmt.Sprintf(certCertFile, taskId, orgId)
		admin = &config.AdminUser{
			OrgId:                tConfig.Chain.OrgId,
			AdminSignKeyFilePath: fmt.Sprintf(certAdminSignKeyFilePath, taskId, orgId),
			AdminSignCrtFilePath: fmt.Sprintf(certAdminSignCrtFilePath, taskId, orgId),
		}

	case "permissionedwithkey":
		tConfig.Chain.UserSignKeyFilePath = fmt.Sprintf(keyUserSignKeyFilePath, taskId, orgId)
		tConfig.Chain.PrivKeyFilePath = fmt.Sprintf(keyPrivKeyFile, taskId, orgId)
		admin = &config.AdminUser{
			OrgId:                tConfig.Chain.OrgId,
			AdminSignKeyFilePath: fmt.Sprintf(keyAdminSignKeyFilePath, taskId, orgId),
		}

	case "public":
		tConfig.Chain.UserSignKeyFilePath = fmt.Sprintf(pubUserSignKeyFilePath, taskId, orgId)
		tConfig.Chain.PrivKeyFilePath = fmt.Sprintf(pubPrivKeyFile, taskId, orgId)
		admin = &config.AdminUser{
			AdminSignKeyFilePath: fmt.Sprintf(pubAdminSignKeyFilePath, taskId, orgId),
		}

	default:
		return "", fmt.Errorf("Unsupported auth type ")
	}

	admins := append(amins, admin)
	tConfig.Chain.AdminUsers = admins

	return
}

func CheckGenesisFile(origId, taskId string, chainInfo *sqlite.TargetChainInfo) (err error) {
	path := fmt.Sprintf(genesisPath, taskId, origId)
	if !filepath.IsAbs(path) {
		path, err = filepath.Abs(path)
		if err != nil {
			return err
		}
	}

	chainconfig, err := chainconf.GetChainConfig(path, "")
	if err != nil {
		return err
	}

	if strings.ToLower(chainconfig.AuthType) != chainInfo.AuthType {
		return fmt.Errorf("The user participation configurations are inconsistent for authType ")
	}

	chainInfo.ChainId = chainconfig.ChainId
	chainInfo.HashAlgo = chainconfig.Crypto.Hash

	if chainconfig.Consensus.Type != 1 {
		return fmt.Errorf("The target chain currently supports only TBFT consensus ")
	}

	if len(chainconfig.TrustRoots) != 1 { //如果不为1
		return fmt.Errorf("The target chain trust-root must one ")
	}

	if len(chainconfig.Consensus.Nodes) != 1 {
		return fmt.Errorf("The target chain consensus node must one ")
	}

	if chainconfig.AuthType != "public" {
		if len(chainconfig.Consensus.Nodes[0].NodeId) != 1 {
			return fmt.Errorf("The target chain consensus node must one ")
		}

		if chainconfig.TrustRoots[0].OrgId != chainInfo.OrgId {
			return fmt.Errorf("The user participation configurations are inconsistent for trust-root orgId ")
		}
	}

	chainInfo.ConsensusNodeId = chainconfig.Consensus.Nodes[0].NodeId[0]

	if chainconfig.AuthType != "public" && chainconfig.Consensus.Nodes[0].OrgId != chainInfo.OrgId {
		return fmt.Errorf("The user participation configurations are inconsistent for consensus orgId ")
	}

	chainInfo.OrgId = chainconfig.Consensus.Nodes[0].OrgId
	chainInfo.GenesisPath = path

	return nil
}

func CreateContract(tConfig *Config) (err error) {
	switch tConfig.OriginChain.ChainType {
	case "fabric":
		tConfig.ContractInfo = config.ContractInfo{
			AssistContractName: "assist",
			TransferMethod:     "transfer",
			UserContractPath:   "./config_path/contract_fabric/transfer",
			AssistContractPath: "./config_path/contract_fabric/assist.7z",
		}
		return nil
	case "chainmaker":

		tConfig.ContractInfo = config.ContractInfo{
			AssistContractName: "assist",
			TransferMethod:     "transfer",
			UserContractPath:   "./config_path/contract_chainmaker/transfer",
			AssistContractPath: "./config_path/contract_chainmaker/assist.7z",
		}
		return nil

	default:

		return fmt.Errorf("Unsupported chain type ")
	}

}

//完成类型与版本，迁移高度的更新。同时设置sdk 路径
func CreateOriginChainConfig(TaskId string, tConfig *Config) (err error) {
	db := sqlite.GetDB()
	task, err := sqlite.GetTaskInfoByTaskId(db, TaskId)
	if err != nil {
		return err
	}

	oChainInfo, err := sqlite.GetOriginChainInfoByTaskId(db, TaskId)
	if err != nil {
		return err
	}

	tConfig.OriginChain = config.OriginChain{
		ChainType:      task.OriginChainType,
		ChainVersion:   task.OriginChainVersion,
		TransferHeight: task.TransferHeight,
	}

	switch task.OriginChainType {
	case "fabric":
		tConfig.OriginChain.Fabric = config.FabricInfo{
			ChainName:  oChainInfo.ChannelName,
			UserName:   oChainInfo.UserName,
			ConfigPath: oChainInfo.SdkPath,
		}
		return nil
	case "chainmaker":
		tConfig.OriginChain.ChainMaker = config.ChainMakerInfo{
			ChainName:  oChainInfo.ChannelName,
			UserName:   oChainInfo.UserName,
			ConfigPath: oChainInfo.SdkPath,
		}
		return nil
	default:

		return fmt.Errorf("Unsupported chain type ")
	}

}

func GetStorage(origId, taskId string, tConfig *Config) (err error) {
	path := fmt.Sprintf(chainMakerPath, taskId, origId)

	if !filepath.IsAbs(path) {
		path, err = filepath.Abs(path)
		if err != nil {
			return err
		}
	}

	conf := &CMConfig{}
	if f, err := os.Open(path); err != nil {
		return err
	} else {
		err = yaml.NewDecoder(f).Decode(conf)
		if err != nil {
			return err
		}
	}

	tConfig.Storage = conf.StorageConfig
	return nil
}

func getDefaultLog(tConfig *Config) {

	tConfig.LogConfig = config.LogConfig{
		LogLevelDefault: "DEBUG",
		LogLevels:       map[string]string{"cm_save": "DEBUG"},
		FilePath:        "./default.log",
		MaxAge:          365,
		RotationTime:    24,
		LogInConsole:    true,
		ShowColor:       true,
	}
}

func getDefaultAsync(tConfig *Config) {
	tConfig.Async = &config.Async{
		QueryCapable:   6,
		ParseCapable:   3,
		RoundNum:       100,
		ThresholdRound: 50,
	}
}
