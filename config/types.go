
package config

type netConfig struct {
	Port      int       `mapstructure:"port"`
	TlsConfig tlsConfig `mapstructure:"tls"`
}

type tlsConfig struct {
	Enable      bool   `mapstructure:"enable"`
	PrivKeyFile string `mapstructure:"priv_key_file"`
	CertFile    string `mapstructure:"cert_file"`
}

type AuthConfig struct {
	Type       string   `mapstructure:"type"`
	Ips        []string `mapstructure:"ips"`
	Parameters Pairs    `parameters:"wxw"`
}

type Pairs struct {
	Key   string `mapstructure:"key"`
	Value string `mapstructure:"value"`
}

type RateLimitConfig struct {
	TokenPerSecond  int64 `mapstructure:"token_per_second"`
	TokenBucketSize int   `mapstructure:"token_bucket_size"`
}

type SuperUserConfig struct {
	UserName string `mapstructure:"name"`
	Password string `mapstructure:"password"`
}

type LogConfig struct {
	LogLevelDefault string            `mapstructure:"log_level_default"`
	LogLevels       map[string]string `mapstructure:"log_levels"`
	FilePath        string            `mapstructure:"file_path"`
	MaxAge          int               `mapstructure:"max_age"`
	RotationTime    int               `mapstructure:"rotation_time"`
	LogInConsole    bool              `mapstructure:"log_in_console"`
	ShowColor       bool              `mapstructure:"show_color"`
}

type DBConfig struct {
	Mysql MysqlConfig `mapstructure:"mysql"`
	Redis RedisConfig `mapstructure:"redis"`
	Mongodb MongoDBConfig `mapstructure:"mongodb"`
}

type MysqlConfig struct {
	UserName  string `mapstructure:"user_name"`
	Password  string `mapstructure:"password"`
	MysqlIp   string `mapstructure:"mysql_ip"`
	MysqlPort int    `mapstructure:"mysql_port"`
	DataBase  string `mapstructure:"data_base"`
	Key       string `mapstructure:"key"`
}

type RedisConfig struct {
	DB        int    `mapstructure:"db"`
	Password  string `mapstructure:"password"`
	RedisIp   string `mapstructure:"redis_ip"`
	RedisPort int    `mapstructure:"redis_port"`
	Key       string `mapstructure:"key"`
	Auth      string `mapstructure:"Auth"`
}

type SzCache struct {
	ErrIndex    uint32   `mapstructure:"err_index"`
	SignSum     uint32   `mapstructure:"sign_sum"`
	Sm4Sum      uint32   `mapstructure:"sm4_sum"`
}

type Chain struct {
	ChainName      string  `mapstructure:"chain_name"`
	ListenUserName string  `mapstructure:"listen_user_name"`
}

type Config struct {
	LogConfig       LogConfig       `mapstructure:"log"`
	CommonConfig    netConfig       `mapstructure:"net"`
	AuthConfig      AuthConfig      `mapstructure:"auth"`
	RateConfig      RateLimitConfig `mapstructure:"rate_limit"`
	SuperUserConfig SuperUserConfig `mapstructure:"super_user"`
	DBConfig        DBConfig        `mapstructure:"db"`
	Chain           Chain           `mapstructure:"chain"`
	SzCache         SzCache         `mapstructure:"sz_cache"`
}

type MongoDBConfig struct {
	UserName string `mapstructure:"user_name"`
	Password string `mapstructure:"password"`
	DbHost   string `mapstructure:"db_host"`
	DbPort   int    `mapstructure:"db_port"`
	DbName   string `mapstructure:"db_name"`
	Auth     bool   `mapstructure:"auth"`
}


