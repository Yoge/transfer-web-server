package db

import (
	"chainmaker.org/chainmaker/transfer-web-server/db/sqlite"
)

const (
	limit = 10
	//statusId = "1"
)

//ContractDBImpl impl provider.ContractDB
type ContractDBImpl struct {
}

func (c *ContractDBImpl) GetContracts(taskId string) (map[string]uint64, error) {
	db := sqlite.GetDB()

	offset := 0
	conn := make(map[string]uint64, 0)
	for i := 0; ; i++ {
		offset += limit * i
		contracts, err := sqlite.GetContractAndPaging(offset, limit, taskId, db)
		if err != nil && err.Error() != "record not found" {
			return nil, err
		}

		for _, v := range contracts {
			conn[v.ContractName] = v.Height
		}
		if len(contracts) < limit {
			return conn, nil
		}
	}

}

func (c *ContractDBImpl) InsertContract(taskId, name string, height uint64) error {
	db := sqlite.GetDB()
	contract := sqlite.Contract{
		ContractName: name,
		Height:       height,
		TaskId:       taskId,
	}
	_, err := contract.InsertContract(db)
	if err != nil {
		return err
	}

	return err
}

//StatusDBImpl impl provider.StatusDB
type StatusDBImpl struct {
}

func (s *StatusDBImpl) UpdateSaveHeightAndNewPre(height uint64, newPre, taskId string) error {
	db := sqlite.GetDB()

	status, err := sqlite.GetStatusByTaskID(db, taskId)
	if err != nil {
		return err
	}

	status.SaveHeight = height
	status.NewPreHash = newPre

	return status.UpdateChain(db)

}

func (s *StatusDBImpl) UpdateUserCrtHash(crtHash, taskId string) error {
	db := sqlite.GetDB()

	status, err := sqlite.GetStatusByTaskID(db, taskId)
	if err != nil {
		return err
	}

	status.EnableShortCert = true
	status.CertHash = crtHash

	return status.UpdateChain(db)

}

func (s *StatusDBImpl) InsertStatus(height uint64, newPre, taskId string) error {
	db := sqlite.GetDB()

	status := &sqlite.Status{
		SaveHeight: height,
		NewPreHash: newPre,
		TaskId:     taskId,
	}

	status, err := status.InsertStatus(db)
	if err != nil {
		return err
	}

	return nil
}

func (s *StatusDBImpl) GetNewPreHash(taskId string) (string, error) {
	db := sqlite.GetDB()
	status, err := sqlite.GetStatusByTaskID(db, taskId)
	if err != nil {
		return "", err
	}

	return status.NewPreHash, nil
}

func (s *StatusDBImpl) GetCertHash(taskId string) (string, error) {
	db := sqlite.GetDB()
	status, err := sqlite.GetStatusByTaskID(db, taskId)
	if err != nil {
		return "", err
	}

	return status.CertHash, nil
}

func (s *StatusDBImpl) GetSaveHeight(taskId string) (uint64, error) {
	db := sqlite.GetDB()
	status, err := sqlite.GetStatusByTaskID(db, taskId)
	if err != nil {
		return 0, err
	}

	return status.SaveHeight, nil
}

//TransferDBImpl contains ContractDBImpl and StatusDBImpl
type TransferDBImpl struct {
	ContractDB *ContractDBImpl // provider.ContractDB
	StatusDB   *StatusDBImpl   //provider.StatusDB
}

func NewTransferDBImpl() *TransferDBImpl {
	contract := &ContractDBImpl{}
	status := &StatusDBImpl{}

	return &TransferDBImpl{
		contract,
		status,
	}
}

func (t *TransferDBImpl) GetContracts(taskId string) (map[string]uint64, error) {
	return t.ContractDB.GetContracts(taskId)
}

func (t *TransferDBImpl) InsertContract(name, taskId string, height uint64) error {
	return t.ContractDB.InsertContract(taskId, name, height)
}

func (t *TransferDBImpl) UpdateSaveHeightAndNewPre(height uint64, newPre, taskId string) error {
	return t.StatusDB.UpdateSaveHeightAndNewPre(height, newPre, taskId)
}

func (t *TransferDBImpl) InsertStatus(height uint64, newPre, taskId string) error {
	return t.StatusDB.InsertStatus(height, newPre, taskId)
}

func (t *TransferDBImpl) GetNewPreHash(taskId string) (string, error) {
	return t.StatusDB.GetNewPreHash(taskId)
}

func (t *TransferDBImpl) GetSaveHeight(taskId string) (uint64, error) {
	return t.StatusDB.GetSaveHeight(taskId)
}

func (t *TransferDBImpl) GetCertHash(taskId string) (string, error) {
	return t.StatusDB.GetCertHash(taskId)
}

func (t *TransferDBImpl) UpdateUserCrtHash(crtHash, taskId string) error {
	return t.StatusDB.UpdateUserCrtHash(crtHash, taskId)
}
