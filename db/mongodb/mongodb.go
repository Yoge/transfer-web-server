///**
//* @Author: starxxliu
//* @Date: 2021/6/28 5:35 下午
// */
//
package mongodb
//
//import (
//	"context"
//	"fmt"
//	"chainmaker.org/chainmaker/transfer-web-server/loggers"
//	"gorm.io/gorm/logger"
//	"time"
//
//	"chainmaker.org/chainmaker/transfer-web-server/config"
//
//	"go.mongodb.org/mongo-driver/mongo"
//	"go.mongodb.org/mongo-driver/mongo/options"
//	"go.uber.org/zap"
//)
//
////MongoFindTimeOut query timeout
//const (
//	MongoFindTimeOut = 2 * time.Second
//)
//
////MongoClient Mongo Client
//var MongoClient *MongoC
//
//func init() {
//	MongoClient = &MongoC{
//		ctx: context.Background(),
//	}
//}
//
//func (mgo *MongoC) SetDatabase(database string) {
//	mgo.database = database
//}
//
//type MongoC struct {
//	ctx      context.Context
//	database string
//	dbc      *mongo.Client
//}
//
//func InitMongo() {
//	var err error
//
//	uri := fmt.Sprintf("mongodb://%s:%d", config.KaiwuConfig.DBConfig.Mongodb.DbHost,
//		config.KaiwuConfig.DBConfig.Mongodb.DbPort)
//
//	ctx := context.TODO()
//
//	credential := options.Credential{
//		AuthSource: config.KaiwuConfig.DBConfig.Mongodb.DbName,
//		Username:   config.KaiwuConfig.DBConfig.Mongodb.UserName,
//		Password:   config.KaiwuConfig.DBConfig.Mongodb.Password,
//	}
//	var clientOpts *options.ClientOptions
//	if config.TransferConfig.DBConfig.MongoDB.Auth {
//		clientOpts = options.Client().ApplyURI(uri).SetAuth(credential)
//	} else {
//		clientOpts = options.Client().ApplyURI(uri)
//	}
//
//	MongoClient.dbc, err = mongo.Connect(ctx, clientOpts)
//	if err != nil {
//		panic(err)
//	}
//
//	// 检查连接
//	err = MongoClient.dbc.Ping(context.TODO(), nil)
//	if err != nil {
//		panic(err)
//	}
//}
//
//func (mgo *MongoC) CreateCollection(collName string) error {
//	err := mgo.dbc.Database(mgo.database).CreateCollection(mgo.ctx, collName)
//	if err != nil {
//		return err
//	}
//	return nil
//}
//
//func (mgo *MongoC) Insert(collName string, doc interface{}) (*mongo.InsertManyResult, error) {
//	many, err := mgo.dbc.Database(mgo.database).Collection(collName).InsertMany(mgo.ctx, []interface{}{doc})
//	if err != nil {
//		return nil, err
//	}
//	return many, nil
//}
//
//func (mgo *MongoC) InsertOne(collName string, doc interface{}) (*mongo.InsertOneResult, error) {
//	one, err := mgo.dbc.Database(mgo.database).Collection(collName).InsertOne(mgo.ctx, doc)
//	if err != nil {
//		return nil, err
//	}
//	return one, nil
//}
//
//func (mgo *MongoC) Update(collName string, filter interface{}, doc interface{}) (*mongo.UpdateResult, error) {
//	res, err := mgo.dbc.Database(mgo.database).Collection(collName).UpdateOne(mgo.ctx, filter, doc)
//	if err != nil {
//		return nil, err
//	}
//	return res, nil
//}
//
//func (mgo *MongoC) FindOne(collName string, filter interface{}) *mongo.SingleResult {
//	ctx, cancel := context.WithTimeout(context.Background(), MongoFindTimeOut)
//	defer cancel()
//	res := mgo.dbc.Database(mgo.database).Collection(collName).FindOne(ctx, filter)
//	return res
//}
//
//func (mgo *MongoC) Find(collName string, filter interface{}, opts ...*options.FindOptions) (*mongo.Cursor, error) {
//	res, err := mgo.dbc.Database(mgo.database).Collection(collName).Find(mgo.ctx, filter, opts...)
//	if err != nil {
//		return nil, err
//	}
//	return res, nil
//}
//
//func (mgo *MongoC) Aggregate(collName string,
//	filter interface{},
//	opts ...*options.AggregateOptions) (*mongo.Cursor, error) {
//	res, err := mgo.dbc.Database(mgo.database).Collection(collName).Aggregate(mgo.ctx, filter, opts...)
//	if err != nil {
//		return nil, err
//	}
//	return res, nil
//}
//
//func (mgo *MongoC) GetCollection(collName string) *mongo.Collection {
//	return mgo.dbc.Database(mgo.database).Collection(collName)
//}
