package sqlite

import "gorm.io/gorm"

type TaskInfo struct {
	Id                 int64  `gorm:"column:id" json:"id"`
	TaskId             string `gorm:"uniqueIndex;column:task_id" json:"task_id"`
	TaskName           string `gorm:"uniqueIndex;column:task_name" json:"task_name"`
	OriginChainType    string `gorm:"column:origin_chain_type" json:"origin_chain_type"`       //原链类型
	OriginChainVersion string `gorm:"column:origin_chain_version" json:"origin_chain_version"` //原链版本
	TargetChainType    string `gorm:"column:target_chain_type" json:"target_chain_type"`       //目标链版本
	TargetChainVersion string `gorm:"column:target_chain_version" json:"target_chain_version"` //目标链类型
	CreateTime         string `gorm:"column:create_time" json:"create_time"`                   //创建时间
	TransferHeight     uint64 `gorm:"column:transfer_height" json:"transfer_height"`           //迁移高度
	Status             int    `gorm:"column:status" json:"status"`                             //1表示初始状态，2表示run 3:表示finish 4:表示stop 5:异常
	AlreadyHeight      uint64 `gorm:"column:already_height" json:"already_height"`             //已经迁移高度
	LastHeight         uint64 `gorm:"column:last_height" json:"last_height"`                   //当前原链最新高度
	StartTime          uint64 `gorm:"column:start_time" json:"start_time"`                     //开始迁移时间
	TransferConfigPath string `gorm:"column:transfer_config_path" json:"transfer_config_path"` //当前任务迁移工具配置信息路径
	LogInfo            string `gorm:"column:log_info" json:"log_info"`                         //记录迁移失败时，log
}

func init() {
	ti := &TaskInfo{}

	RegisterTable(ti)
}

func (ti *TaskInfo) TableName() string {
	return "task_info"
}

func (ti *TaskInfo) InsertTaskInfo(conn *gorm.DB) (*TaskInfo, error) {
	err := conn.Table(ti.TableName()).Create(ti).Error
	return ti, err
}

func QueryTasksAndPaging(offset, limit int, conn *gorm.DB) ([]*TaskInfo, error) {
	var tasks []*TaskInfo
	err := conn.Table("task_info").Limit(limit).Offset(offset).Find(&tasks).Error

	return tasks, err
}

func QueryTasksPagingByStatus(offset, limit int, conn *gorm.DB, status int) ([]*TaskInfo, error) {
	var tasks []*TaskInfo
	err := conn.Table("task_info").Where("status = ?", status).Limit(limit).Offset(offset).Find(&tasks).Error

	return tasks, err
}

func QueryTasksPagingByName(offset, limit int, conn *gorm.DB, name string, taskId string) ([]*TaskInfo, error) {
	var tasks []*TaskInfo
	err := conn.Table("task_info").Where("task_name LIKE ? and task_id LIKE ?", "%"+name+"%", "%"+taskId+"%").Limit(limit).Offset(offset).Find(&tasks).Error
	//err := conn.Table("task_info").Limit(limit).Offset(offset).Find(&tasks).Error
	return tasks, err
}
func QueryTasksPagingByNameCount(conn *gorm.DB, name string, taskId string) (int64, error) {
	var count int64
	err := conn.Table("task_info").Where("task_name LIKE ?", "%"+name+"%", "%"+taskId+"%").Count(&count).Error

	return count, err
}

func QueryTasksPagingByNameAndStatusAndTaskId(offset, limit int, conn *gorm.DB, name string, status int, taskId string) ([]*TaskInfo, error) {
	var tasks []*TaskInfo
	err := conn.Table("task_info").Where("task_name LIKE ? and status = ? and task_id LIKE ?", "%"+name+"%", status, "%"+taskId+"%").Limit(limit).Offset(offset).Find(&tasks).Error
	return tasks, err
}

func QueryTasksPagingByNameAndStatusAndTaskIdCount(conn *gorm.DB, name string, status int, taskId string) (int64, error) {
	var count int64
	err := conn.Table("task_info").Where("task_name LIKE ? and status = ? and task_id LIKE ?", "%"+name+"%", status, "%"+taskId+"%").Count(&count).Error

	return count, err
}

func GetTaskInfo(conn *gorm.DB, id string) (*TaskInfo, error) {
	var taskInfo TaskInfo
	err := conn.Where("id = ?", id).First(&taskInfo).Error

	return &taskInfo, err
}

func GetTaskInfoByTaskId(conn *gorm.DB, taskId string) (*TaskInfo, error) {
	var taskInfo TaskInfo
	err := conn.Where("task_id = ?", taskId).First(&taskInfo).Error

	return &taskInfo, err
}

func (ti *TaskInfo) UpdateChain(conn *gorm.DB) error {
	err := conn.Model(ti).Save(ti).Error
	return err
}

func DropTaskInfo(conn *gorm.DB) error {
	return conn.Exec("DROP TABLE task_info").Error
}

func (ti *TaskInfo) QueryTasksCount(conn *gorm.DB) (int64, error) {
	var count int64
	conn.Table(ti.TableName()).Count(&count)
	return count, nil
}

func (ti *TaskInfo) QueryTasksCountByStatus(conn *gorm.DB, status int) (int64, error) {
	var count int64
	err := conn.Table(ti.TableName()).Where("status = ?", status).Count(&count).Error
	return count, err
}

func (ti *TaskInfo) QueryTasksCountByChainType(conn *gorm.DB, chainType string) ([]TaskInfo, error) {
	var tasks []TaskInfo
	err := conn.Table(ti.TableName()).Where("origin_chain_type = ?", chainType).Find(&tasks).Error
	return tasks, err
}
