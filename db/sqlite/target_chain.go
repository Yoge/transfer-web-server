package sqlite

import "gorm.io/gorm"

type TargetChainInfo struct {
	Id              int64  `gorm:"column:id"`
	TaskId          string `gorm:"uniqueIndex;column:task_id"`
	ChainId         string `gorm:"column:chain_id"`
	OrgId           string `gorm:"column:org_id"`
	ConsensusNodeId string `gorm:"column:consensus_node_id"`
	AuthType        string `gorm:"column:auth_type"`
	HashAlgo        string `gorm:"column:hash_algo"`
	GenesisPath     string `gorm:"column:genesis_path"`
	ConfigPath      string `gorm:"column:config_path"` //.zip 压缩包配置信息路径
}

func init() {
	tci := &TargetChainInfo{}

	RegisterTable(tci)
}

func (tci *TargetChainInfo) TableName() string {
	return "target_chain_info"
}

func (tci *TargetChainInfo) InsertTargetChainInfo(conn *gorm.DB) (*TargetChainInfo, error) {
	err := conn.Table(tci.TableName()).Create(tci).Error
	return tci, err
}

//func GetStatusAndPaging(offset, limit int, conn *gorm.DB) ([]*Status, error) {
//	var certs []*Status
//	err := conn.Table("status").Limit(limit).Offset(offset).Find(&certs).Error
//
//	return certs, err
//}

func GetTargetChainInfoByTaskId(conn *gorm.DB, id string) (*TargetChainInfo, error) {
	var target TargetChainInfo
	err := conn.Where("task_id = ?", id).First(&target).Error
	return &target, err
}

func (tci *TargetChainInfo) UpdateChain(conn *gorm.DB) error {
	err := conn.Model(tci).Updates(tci).Error
	return err
}

func DropTargetChainInfo(conn *gorm.DB) error {
	return conn.Exec("DROP TABLE target_chain_info").Error
}
