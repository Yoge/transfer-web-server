/**
 * @Author: starxxliu
 * @Date: 2021/12/29 9:51 下午
 */

package mulnode

import (
	"context"

	commonpb "chainmaker.org/chainmaker/pb-go/v2/common"
	consensuspb "chainmaker.org/chainmaker/pb-go/v2/consensus"
	storePb "chainmaker.org/chainmaker/pb-go/v2/store"
	"chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-tool/loggers"
	"github.com/gogo/protobuf/proto"
)

var (
	threshold = 1
	isBatch   = false
)

type NodeManage struct {
	Nodes     map[string]*Node
	chans     map[string]chan [][]byte
	log       *loggers.CMLogger
	ctx       context.Context
	pending   [][]byte
	threshold int
	isBatch   bool
}

// NewNodeManage 构建对应的node instance
func NewNodeManage(ctx context.Context, nodes []*config.NodeInfo, log *loggers.CMLogger, errChan chan error) *NodeManage {
	manage := &NodeManage{
		chans: make(map[string]chan [][]byte),
		Nodes: make(map[string]*Node),
		log:   log,
		ctx:   ctx,
	}

	for _, info := range nodes {
		send := make(chan [][]byte, 1024)
		manage.chans[info.NodeId] = send
		node := NewNode(info.Ip, info.Port, log, send, ctx, errChan)
		manage.Nodes[info.NodeId] = node
	}

	return manage
}

func (n *NodeManage) Start() {
	for _, node := range n.Nodes {
		node.Start()
	}
}

// PutBlock put block
func (n *NodeManage) PutBlock(proposal *consensuspb.ProposalBlock, to string) error {

	return nil
}

// GetLastBlockHeight get last block
func (n *NodeManage) GetLastBlockHeight(to string) *commonpb.Block {

	return nil
}

// GetBlockHeightByHeight get block
func (n *NodeManage) GetBlockHeightByHeight(h uint64, to string) *storePb.BlockWithRWSet {

	return nil
}

// InitGenesis initialize genesis block
func (n *NodeManage) InitGenesis(genesisBlock *commonpb.Block, rwSetList []*commonpb.TxRWSet) error {

	return nil
}

// PutBlocks put block
func (n *NodeManage) PutBlocks(block *commonpb.Block, txRWSets []*commonpb.TxRWSet) error {
	blockAndRw := &storePb.BlockWithRWSet{
		Block:    block,
		TxRWSets: txRWSets,
	}
	//批量与非批量的控制
	var pb []byte
	var err error
	pb, err = proto.Marshal(blockAndRw)
	if err != nil {
		return err
	}
	n.pending = append(n.pending, pb)

	if len(n.pending) > n.threshold {
		for _, send := range n.chans {
			send <- n.pending
		}

		n.pending = make([][]byte, 0, n.threshold)
	}

	return nil
}

// GetLastBlockHeights get last block
func (n *NodeManage) GetLastBlockHeights() (*commonpb.Block, error) {
	var tempBlock *commonpb.Block
	for id, node := range n.Nodes {
		block, err := node.GetLastBlockHeight()
		if err != nil {
			n.log.Errorf("node[%s] GetLastBlockHeights failed,err[%s]", id, err.Error())
			return nil, err
		} else if block == nil {
			continue
		}
		n.log.Infof("node[%s] block last height[%d]", block.Header.BlockHeight)
		if tempBlock == nil {
			tempBlock = block
		} else if tempBlock.Header.BlockHeight > block.Header.BlockHeight {
			tempBlock = block
		}
	}

	return tempBlock, nil
}

// GetBlockWithRWSetsByHeights get block
func (n *NodeManage) GetBlockWithRWSetsByHeights(h uint64) (*storePb.BlockWithRWSet, error) {
	for id, node := range n.Nodes {
		block, err := node.GetBlockWithRWSetsByHeight(h)
		if err != nil {
			n.log.Errorf("node[%s] GetBlockWithRWSetsByHeights failed,height[%d],err[%s]", id, h, err.Error())
			return nil, err
		}
		return block, nil
	}
	return nil, nil
}

// InitGenesiss initialize genesis block
func (n *NodeManage) InitGenesiss(block *storePb.BlockWithRWSet) error {
	gb, err := proto.Marshal(block)
	if err != nil {
		return err
	}
	for id, node := range n.Nodes {
		err := node.InitGenesis(gb)
		if err != nil {
			n.log.Panic(id, " node init genesis failed,", err)
		}
	}

	return nil
}
