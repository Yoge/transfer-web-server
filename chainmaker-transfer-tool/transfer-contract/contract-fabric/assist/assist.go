/**
 * @Author: starxxliu
 * @Date: 2021/11/29 6:50 下午
 */

package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"strings"
	"unicode/utf8"

	"chainmaker.org/chainmaker-contract-sdk-docker-go/pb/protogo"
	"chainmaker.org/chainmaker-contract-sdk-docker-go/shim"
	"github.com/golang/protobuf/proto"
	"github.com/hyperledger/fabric-protos-go/common"
	"github.com/hyperledger/fabric-protos-go/peer"
	"github.com/transfer-contract/assist/third_party/github.com/hyperledger/fabric/core/ledger/kvledger/txmgmt/rwsetutil"
)

const (
	defaultVersion = "v1.0.0"
	SysLscc        = "lscc"

	minUnicodeRuneValue   = 0            //U+0000
	maxUnicodeRuneValue   = utf8.MaxRune //U+10FFFF - maximum (and unallocated) code point
	compositeKeyNamespace = "\x00"
	chainMakerSeparate    = "#"
	newContractSeparate   = "_"
)

type AssistContract struct {
}

// KVWrite captures a write (update/delete) operation performed during transaction simulation
type KVWrite struct {
	Key                  string   `protobuf:"bytes,1,opt,name=key,proto3" json:"key,omitempty"`
	IsDelete             bool     `protobuf:"varint,2,opt,name=is_delete,json=isDelete,proto3" json:"is_delete,omitempty"`
	Value                []byte   `protobuf:"bytes,3,opt,name=value,proto3" json:"value,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

// KVWrite captures a write (update/delete) operation performed during transaction simulation
type KVWrite2 struct {
	Key      string `protobuf:"bytes,1,opt,name=key,proto3" json:"key,omitempty"`
	IsDelete bool   `protobuf:"varint,2,opt,name=is_delete,json=isDelete,proto3" json:"is_delete,omitempty"`
	Value    []byte `protobuf:"bytes,3,opt,name=value,proto3" json:"value,omitempty"`
}

// 部署合约 需要反向验证在fabric install 实力化交易时要怎么做
func (f *AssistContract) InitContract(stub shim.CMStubInterface) protogo.Response {
	return shim.Success([]byte("Init Success"))
}

// 调用合约
func (f *AssistContract) InvokeContract(stub shim.CMStubInterface) protogo.Response {

	// 获取参数，方法名通过参数传递
	method := stub.GetArgs()["method"]

	switch string(method) {
	case "transfer":
		return transfer(stub)
	default:
		return shim.Error("invalid method")
	}
}

func transfer(stub shim.CMStubInterface) protogo.Response {
	args := stub.GetArgs()
	tx_type := args["tx_type"]
	is_valid := args["is_valid"]

	if string(tx_type) == "config" || string(is_valid) == "invalid" {
		return shim.Success(nil)
	}

	origin_tx := args["origin_tx"]
	wSet, err := parseFabricTx(origin_tx)
	if err != nil {
		shim.Error("parseFabricTx happen failed,err :" + err.Error())
	}

	for k, v := range wSet {
		stub.Log(k)
		cwset := map[string][]byte{"tx_wset": v}

		//debug
		wSet2 := make([]*KVWrite2, 0)

		err := json.Unmarshal(v, &wSet2)
		if err != nil {
			return shim.Error("unmarshal tx_wset err: " + err.Error())
		}

		for _, v1 := range wSet2 {
			stub.Log("==assist==" + v1.Key)
		}

		res := stub.CallContract(k, defaultVersion, cwset)
		if res.Status != shim.OK {
			shim.Error("assis call contract " + k + " happen err:" + res.Message)
		}
	}
	return shim.Success(nil)
}

// 这里只关注wSet,然后将所有的wset修改为,可以直接执行callContract的结构
func parseFabricTx(txByte []byte) (map[string][]byte, error) {
	env := &common.Envelope{}
	err := proto.Unmarshal(txByte, env)
	if err != nil {
		return nil, err
	}

	payload := &common.Payload{}
	err = proto.Unmarshal(env.Payload, payload)
	if err != nil {
		return nil, err
	}

	trans := &peer.Transaction{}
	err = proto.Unmarshal(payload.Data, trans)
	if err != nil {
		return nil, err
	}

	if len(trans.Actions) < 1 {
		return nil, errors.New("no transaction in block")
	}

	ccp := &peer.ChaincodeActionPayload{}
	err = proto.Unmarshal(trans.Actions[0].Payload, ccp)
	if err != nil {
		return nil, err
	}

	ccresp := &peer.ProposalResponsePayload{}
	err = proto.Unmarshal(ccp.Action.ProposalResponsePayload, ccresp)
	if err != nil {
		return nil, err
	}

	cca := &peer.ChaincodeAction{}
	err = proto.Unmarshal(ccresp.Extension, cca)
	if err != nil {
		return nil, err
	}

	txRWSet := &rwsetutil.TxRwSet{}
	if err = txRWSet.FromProtoBytes(cca.Results); err != nil {
		return nil, fmt.Errorf("%s , txRWSet.FromProtoBytes failed", err)
	}

	return getWSet(txRWSet)
}

func getWSet(txRwSet *rwsetutil.TxRwSet) (map[string][]byte, error) {
	wSet := make(map[string][]byte, 0)
	var err error
	for _, v := range txRwSet.NsRwSets {
		if filterSysContractName(v.NameSpace) {
			continue
		}
		for _, wSet := range v.KvRwSet.Writes {
			wSet.Key, err = FixKey(wSet.Key)
			if err != nil {
				return nil, err
			}
		}

		wsetByte, err := json.Marshal(v.KvRwSet.Writes)
		if err != nil {
			return nil, err
		}

		wSet[v.NameSpace] = wsetByte
	}
	return wSet, nil
}

func filterSysContractName(name string) bool {
	if name == SysLscc {
		return true
	}
	return false
}

//fixKey 修改fabric的联合主键，变为chainmaker 可以使用的
//由于chainmaker 只支持一级field,而fabric 却没有这个限制，所以我们将fabric的所有联合主键合并为一个field,中间使用
//'#'作为分隔符
func FixKey(key string) (string, error) {
	key, attributes, err := splitCompositeKey(key)
	if err != nil {
		return "", err
	}
	if len(attributes) == 0 {
		return key, nil
	}
	key = strings.TrimPrefix(key, compositeKeyNamespace) + chainMakerSeparate
	for _, v := range attributes {
		key += v + newContractSeparate
	}

	key = strings.TrimSuffix(key, newContractSeparate)
	return key, nil
}

func splitCompositeKey(compositeKey string) (string, []string, error) {
	componentIndex := 1
	components := []string{}
	for i := 1; i < len(compositeKey); i++ {
		if compositeKey[i] == minUnicodeRuneValue {
			components = append(components, compositeKey[componentIndex:i])
			componentIndex = i + 1
		}
	}
	if len(components) == 0 {
		return compositeKey, components, nil
	}

	return components[0], components[1:], nil
}

func main() {
	err := shim.Start(new(AssistContract))
	if err != nil {
		log.Fatal(err)
	}
}
