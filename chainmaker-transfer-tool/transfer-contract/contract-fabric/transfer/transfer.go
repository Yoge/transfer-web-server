/**
 * @Author: starxxliu
 * @Date: 2021/11/29 8:32 下午
 */

package main

import (
	"encoding/json"
	"log"

	"chainmaker.org/chainmaker-contract-sdk-docker-go/pb/protogo"
	"chainmaker.org/chainmaker-contract-sdk-docker-go/shim"
)

const defaultVersion = "v1.0.0"

type TransferContract struct {
}

// KVWrite captures a write (update/delete) operation performed during transaction simulation
type KVWrite struct {
	Key      string `protobuf:"bytes,1,opt,name=key,proto3" json:"key,omitempty"`
	IsDelete bool   `protobuf:"varint,2,opt,name=is_delete,json=isDelete,proto3" json:"is_delete,omitempty"`
	Value    []byte `protobuf:"bytes,3,opt,name=value,proto3" json:"value,omitempty"`
}

// 部署合约 需要反向验证在fabric install 实力化交易时要怎么做
func (f *TransferContract) InitContract(stub shim.CMStubInterface) protogo.Response {
	args := stub.GetArgs()

	wSet := make(map[string][]byte, 0)
	tx_wset := args["tx_wset"]

	if tx_wset == nil {
		return shim.Success(nil)
	}
	err := json.Unmarshal(tx_wset, &wSet)
	if err != nil {
		return shim.Error("transfer unmarshal happen err: " + err.Error())
	}

	for k, v := range wSet { //需要确定当是调用自己时候
		stub.Log("===k debug===" + k)

		cwset := map[string][]byte{"tx_wset": v}

		res := stub.CallContract(k, defaultVersion, cwset)
		if res.Status != shim.OK {
			shim.Error("call contract " + k + " happen err:" + res.Message)
		}
	}
	return shim.Success(nil)
}

// 调用合约  不支持多级的复合主键
func (f *TransferContract) InvokeContract(stub shim.CMStubInterface) protogo.Response {

	// 获取参数，方法名通过参数传递
	args := stub.GetArgs()
	tx_wset := args["tx_wset"]
	if tx_wset == nil {
		stub.Log("===debug tx_wset===")
		return shim.Success(nil)
	}

	wSet := make([]*KVWrite, 0)

	err := json.Unmarshal(tx_wset, &wSet)
	if err != nil {
		return shim.Error("unmarshal tx_wset err: " + err.Error())
	}

	for _, v := range wSet {
		stub.Log("==debug key==" + v.Key)
		if v.IsDelete {
			err := stub.DelStateFromKey(v.Key)
			if err != nil {
				return shim.Error(err.Error())
			}
		} else {
			err := stub.PutStateByte(v.Key, "", v.Value)
			if err != nil {
				return shim.Error(err.Error())
			}
		}
	}
	return shim.Success(nil)
}

func main() {
	err := shim.Start(new(TransferContract))
	if err != nil {
		log.Fatal(err)
	}
}
