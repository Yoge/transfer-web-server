/**
 * @Author: starxxliu
 * @Date: 2022/3/17 4:27 下午
 */

package provider

import commonPb "chainmaker.org/chainmaker/pb-go/v2/common"

type ContractSim interface {
	//Simulation contract simulation execute
	Simulation(txWriteKeyMap map[string]*commonPb.TxWrite, wSet map[string][]byte, txRwSet interface{}) error
}
