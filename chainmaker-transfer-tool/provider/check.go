/**
 * @Author: starxxliu
 * @Date: 2022/3/17 2:41 下午
 */

package provider

type Checker interface {
	//CheckBlock check origin and target block wset
	CheckBlock(info interface{}) ([]string, error)
}
