///**
// * @Author: starxxliu
// * @Date: 2021/11/20 4:57 下午
// */
//
package main

import (
	"flag"
	"os"
	"os/signal"
	"syscall"

	"chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-tool/core"
	"chainmaker.org/chainmaker/transfer-tool/db"
	"chainmaker.org/chainmaker/transfer-tool/db/sqlite"
	"chainmaker.org/chainmaker/transfer-tool/loggers"
	"go.uber.org/zap"
)

var (
	logger                 *zap.SugaredLogger
	configPath, sqlitePath string // 相对于调用spv二进制的路径，即src
)

func main() {
	flag.StringVar(&configPath, "configPath", "./config_path", "The path is config path (if set)")
	flag.StringVar(&sqlitePath, "sqlPath", "./sqlitedb/sqlite.db", "The path is sqlite db path (if set)")

	flag.Parse()

	start()
}

func start() {
	//初始化config
	config.InitConfig(configPath, config.GetConfigEnv())

	//sqlite init
	sqlite.StartSqliteConnect(sqlitePath)

	loggers.SetLogConfig(&config.TransferConfig.LogConfig)

	logger = loggers.GetLogger(loggers.MODULE_MAIN)

	transferDB := db.NewTransferDBImpl()
	tc := config.TransferConfig

	// new an error channel to receive errors
	errorC := make(chan error, 1)
	// handle exit signal in separate go routines
	go handleExitSignal(errorC)

	transfer, err := core.NewTransfer(tc, transferDB, nil, errorC, "")
	if err != nil {
		logger.Error(err)
		return
	}

	err = transfer.Start()
	if err != nil {
		logger.Error(err)
		return
	}

	//lock
	err = <-errorC
	if err != nil {
		logger.Error("transfer-tool encounters error ", err)
	}

	transfer.Stop()

	sqlite.Close()

	logger.Infof("close transfer-tool ! ")
}

func handleExitSignal(exitC chan<- error) {

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGTERM, os.Interrupt, syscall.SIGINT)
	defer signal.Stop(signalChan)

	for sig := range signalChan {
		logger.Infof("received signal: %d (%s)", sig, sig)
		exitC <- nil
	}
}
