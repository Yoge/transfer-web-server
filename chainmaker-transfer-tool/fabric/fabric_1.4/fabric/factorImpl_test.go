/**
 * @Author: starxxliu
 * @Date: 2021/12/17 3:59 下午
 */

package fabric

import (
	"io/ioutil"
	"strconv"
	"testing"

	"chainmaker.org/chainmaker/transfer-tool/config"
	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/require"
)

func TestQueryFabricBlock(t *testing.T) {
	height := uint64(0)
	config.InitConfig("../../config_path", config.GetConfigEnv())

	fetcher, err := NewFetcher(0, nil, config.TransferConfig.Fabric.ChainName,
		config.TransferConfig.Fabric.UserName, "../../config_path/dev/zxl_sdk_config.yml", nil, nil)
	require.Nil(t, err)

	block, err := fetcher.QueryBlockByHeight(height) //query fabric block for height
	require.Nil(t, err)

	blockByte, err := proto.Marshal(block)
	require.Nil(t, err)

	fileName := "../../config_path/testdata/" + strconv.Itoa(int(height)) + ".proto"
	err = ioutil.WriteFile(fileName, blockByte, 777)
	require.Nil(t, err)
}
