/**
 * @Author: starxxliu
 * @Date: 2022/1/7 5:51 下午
 */

package fabric

import (
	"io/ioutil"
	"strconv"
	"testing"

	"github.com/golang/protobuf/proto"
	"github.com/hyperledger/fabric-protos-go/common"
	"github.com/stretchr/testify/require"
)

/*
   测试区块场景
   1）genesis block
   2) config block
   3）invalid block (尽量覆盖全部无效交易流程)
   4）正常block
   5）instant chaincode block
   6) upgrade chaincode block(待定)
*/
func TestParseBlock(t *testing.T) {
	//1: test genesis block
	height := 0
	fileName := "../../config_path/testdata/fabric_" + strconv.Itoa(int(height)) + ".proto"
	blockByte0, err := ioutil.ReadFile(fileName)
	require.Nil(t, err)

	block0 := &common.Block{}
	err = proto.Unmarshal(blockByte0, block0)
	require.Nil(t, err)

	parseBloc0, err := ParseBlock(block0)
	require.Nil(t, err)
	println(parseBloc0)
	//2: test instant fabric block
	height = 1
	fileName = "../../config_path/testdata/fabric_" + strconv.Itoa(int(height)) + ".proto"
	blockByte1, err := ioutil.ReadFile(fileName)
	require.Nil(t, err)

	block1 := &common.Block{}
	err = proto.Unmarshal(blockByte1, block1)
	require.Nil(t, err)

	parseBloc1, err := ParseBlock(block1)
	require.Nil(t, err)
	println(parseBloc1)

	//3: test normal fabric block
	height = 2
	fileName = "../../config_path/testdata/fabric_" + strconv.Itoa(int(height)) + ".proto"
	blockByte2, err := ioutil.ReadFile(fileName)
	require.Nil(t, err)

	block2 := &common.Block{}
	err = proto.Unmarshal(blockByte2, block2)
	require.Nil(t, err)

	parseBloc2, err := ParseBlock(block2)
	require.Nil(t, err)
	println(parseBloc2)

	//4: test sig error fabric block
	height = 197672
	fileName = "../../config_path/testdata/fabric_" + strconv.Itoa(int(height)) + ".proto"
	blockByte3, err := ioutil.ReadFile(fileName)
	require.Nil(t, err)

	block3 := &common.Block{}
	err = proto.Unmarshal(blockByte3, block3)
	require.Nil(t, err)

	parseBloc3, err := ParseBlock(block3)
	require.Nil(t, err)
	println(parseBloc3)

	//5: test tx duplicate fabric block
	height = 304079
	fileName = "../../config_path/testdata/fabric_" + strconv.Itoa(int(height)) + ".proto"
	blockByte4, err := ioutil.ReadFile(fileName)
	require.Nil(t, err)

	block4 := &common.Block{}
	err = proto.Unmarshal(blockByte4, block4)
	require.Nil(t, err)

	parseBloc4, err := ParseBlock(block4)
	require.Nil(t, err)
	println(parseBloc4)

}
