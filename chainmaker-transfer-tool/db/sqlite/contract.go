/**
 * @Author: starxxliu
 * @Date: 2021/11/21 1:45 下午
 */

package sqlite

import "gorm.io/gorm"

type Contract struct {
	Id           int64  `gorm:"column:id"`
	ContractName string `gorm:"uniqueIndex;column:contract_name"`
	Height       uint64 `gorm:"column:height"`
}

func init() {
	con := &Contract{}

	RegisterTable(con)
}

func (u *Contract) TableName() string {
	return "contract"
}

func (u *Contract) InsertContract(conn *gorm.DB) (*Contract, error) {
	err := conn.Table(u.TableName()).Create(u).Error
	return u, err
}

func GetContractAndPaging(offset, limit int, conn *gorm.DB) ([]*Contract, error) {
	var certs []*Contract
	err := conn.Table("contract").Limit(limit).Offset(offset).Find(&certs).Error

	return certs, err
}

func DropContract(conn *gorm.DB) error {
	return conn.Exec("DROP TABLE contract").Error
}
