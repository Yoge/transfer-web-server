/**
 * @Author: p_starxxliu
 * @Date: 2021/4/17 2:05 下午
 */

package sqlite

import (
	"log"

	"go.uber.org/zap"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var (
	mysqlDB = &gorm.DB{}

	tableInstance = make([]interface{}, 0)

	logger *zap.SugaredLogger
)

func StartSqliteConnect(path string) {
	//logger = loggers.GetLogger(loggers.MODULE_MANAGER)

	//logger.Debug("start sqlite connect")

	if path == "" {
		path = "./sqlite.db"
	}

	db, err := gorm.Open(sqlite.Open(path), &gorm.Config{})

	if err != nil {
		log.Fatal("Sqlite db start err : " + err.Error())
	}

	mysqlDB = db

	createTable(db)
}

//RegisterTable  register table
func RegisterTable(value interface{}) {
	tableInstance = append(tableInstance, value)
}

func createTable(db *gorm.DB) {
	for _, v := range tableInstance {
		if v == nil {
			continue
		}

		if !db.Migrator().HasTable(v) {
			err := db.AutoMigrate(v)
			if err != nil {
				logger.Fatal("create tables failed ", err.Error())
			}
		}
	}
}

func GetDB() *gorm.DB {
	return mysqlDB
}

func Close() {
	sqlDB, err := mysqlDB.DB()
	if err != nil {
		panic("close mysql db err: " + err.Error())
	}
	sqlDB.Close()
}
