/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package chainmaker_sdk_go

import (
	"context"
	"io"

	"chainmaker.org/chainmaker-go/pb/protogo/api"
	"chainmaker.org/chainmaker-go/pb/protogo/common"
	"github.com/gogo/protobuf/proto"
	"google.golang.org/grpc"
	grpccodes "google.golang.org/grpc/codes"
	grpcstatus "google.golang.org/grpc/status"
)

func (cc *ChainClient) SubscribeBlock(ctx context.Context, startBlock, endBlock int64,
	withRwSet bool) (<-chan interface{}, error) {
	payloadBytes, err := constructSubscribeBlockPayload(startBlock, endBlock, withRwSet)
	if err != nil {
		return nil, err
	}

	return cc.Subscribe(ctx, common.TxType_SUBSCRIBE_BLOCK_INFO, payloadBytes)
}

func (cc *ChainClient) SubscribeContractEvent(ctx context.Context, startBlock, endBlock int64,
	contractName, topic string) (<-chan interface{}, error) {
	payloadBytes, err := constructSubscribeContractEventPayload(startBlock, endBlock, contractName, topic)
	if err != nil {
		return nil, err
	}

	return cc.Subscribe(ctx, common.TxType_SUBSCRIBE_CONTRACT_EVENT_INFO, payloadBytes)
}

func (cc *ChainClient) SubscribeTx(ctx context.Context, startBlock, endBlock int64, txType common.TxType, txIds []string) (<-chan interface{}, error) {
	payloadBytes, err := constructSubscribeTxPayload(startBlock, endBlock, txType, txIds)
	if err != nil {
		return nil, err
	}

	return cc.Subscribe(ctx, common.TxType_SUBSCRIBE_TX_INFO, payloadBytes)
}

func (cc *ChainClient) Subscribe(ctx context.Context, txType common.TxType, payloadBytes []byte) (<-chan interface{}, error) {
	// get stream first time, throw immediately when an error occurs
	stream, err := cc.getSubscribeStream(ctx, txType, payloadBytes)
	if err != nil {
		cc.logger.Error(err)
		return nil, err
	}

	var (
		dataC             = make(chan interface{})
		reconnectC, doneC = make(chan struct{}, 1), make(chan struct{}, 1)
	)

	go func() {
		defer func() {
			close(dataC)
			close(reconnectC)
			close(doneC)
		}()

		go cc.subscribe(ctx, txType, dataC, reconnectC, doneC, stream)
		for {
			select {
			case <-reconnectC:
				cc.logger.Debug("[SDK] Subscriber reconnecting...")
				// always get a new stream
				stream, err := cc.getSubscribeStream(ctx, txType, payloadBytes)
				if err != nil {
					cc.logger.Error(err)
					return
				}
				go cc.subscribe(ctx, txType, dataC, reconnectC, doneC, stream)
			case <-doneC:
				cc.logger.Debug("[SDK] Subscriber done")
				return
			}
		}
	}()

	return dataC, nil
}

func (cc *ChainClient) subscribe(ctx context.Context, txType common.TxType, dataC chan interface{},
	reconnectC, doneC chan struct{}, stream api.RpcNode_SubscribeClient) {

	for {
		select {
		case <-ctx.Done():
			doneC <- struct{}{}
			return
		default:
			result, err := stream.Recv()
			if err == io.EOF {
				cc.logger.Debugf("[SDK] Subscriber got EOF and stop recv msg")
				doneC <- struct{}{}
				return
			}

			if err != nil {
				cc.logger.Errorf("[SDK] Subscriber receive failed, %s", err)
				rpcStatus, ok := grpcstatus.FromError(err)
				if !ok {
					doneC <- struct{}{}
					return
				}

				if rpcStatus.Code() != grpccodes.Unavailable {
					doneC <- struct{}{}
					return
				}

				reconnectC <- struct{}{}
				return
			}

			var ret interface{}
			switch txType {
			case common.TxType_SUBSCRIBE_BLOCK_INFO:
				blockInfo := &common.BlockInfo{}
				if err = proto.Unmarshal(result.Data, blockInfo); err != nil {
					cc.logger.Error("[SDK] Subscriber receive block failed, %s", err)
					doneC <- struct{}{}
					return
				}

				ret = blockInfo
			case common.TxType_SUBSCRIBE_TX_INFO:
				tx := &common.Transaction{}
				if err = proto.Unmarshal(result.Data, tx); err != nil {
					cc.logger.Error("[SDK] Subscriber receive tx failed, %s", err)
					doneC <- struct{}{}
					return
				}
				ret = tx
			case common.TxType_SUBSCRIBE_CONTRACT_EVENT_INFO:
				events := &common.ContractEventInfoList{}
				if err = proto.Unmarshal(result.Data, events); err != nil {
					cc.logger.Error("[SDK] Subscriber receive contract event failed, %s", err)
					doneC <- struct{}{}
					return
				}
				for _, event := range events.ContractEvents {
					dataC <- event
				}
				continue

			default:
				ret = result.Data
			}

			dataC <- ret
		}
	}
}

func (cc *ChainClient) getSubscribeStream(ctx context.Context, txType common.TxType,
	payloadBytes []byte) (api.RpcNode_SubscribeClient, error) {

	req, err := cc.generateTxRequest(GetRandTxId(), txType, payloadBytes)
	if err != nil {
		return nil, err
	}

	networkCli, err := cc.pool.getClient()
	if err != nil {
		return nil, err
	}

	return networkCli.rpcNode.Subscribe(ctx, req, grpc.MaxCallSendMsgSize(networkCli.rpcMaxSendMsgSize))
}
