package sim_contract

import (
	commonv1 "chainmaker.org/chainmaker-go/pb/protogo/common"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/transfer-tool/common"
	"github.com/gogo/protobuf/proto"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/core/ledger/kvledger/txmgmt/rwsetutil"
)

var (
	paramWSetName = "tx_wset"
	OriginTx      = "origin_tx"
	IsValid       = "is_valid"
	InValid       = "invalid"
)

func CreateWriteKeyMap(txWriteKeyMap map[string]*commonPb.TxWrite, wSet map[string][]byte) error {
	//得到chainmaker1.x tx读写集
	rwsetBytes := wSet["origin_rwset"]
	if rwsetBytes == nil {
		return nil
	}

	//var txrwsetv1 *commonv1.TxRWSet
	txrwsetv1 := &commonv1.TxRWSet{}

	err := proto.Unmarshal(rwsetBytes, txrwsetv1)
	if err != nil {
		return err
	}

	txwsetv1 := txrwsetv1.TxWrites

	for _, txWrite := range txwsetv1 {

		txWriteKeyMap[common.ConstructKey(txWrite.ContractName, txWrite.Key)] = &commonPb.TxWrite{
			Key:          txWrite.Key,
			Value:        txWrite.Value,
			ContractName: txWrite.ContractName,
		}
	}
	return nil
}

func CreateTxWriteKeyMap(txWriteKeyMap map[string]*commonPb.TxWrite, wSet map[string][]byte, txRwSet *rwsetutil.TxRwSet) error {
	//得到chainmaker1.x tx读写集
	rwsetBytes := wSet["origin_rwset"]
	if rwsetBytes == nil {
		return nil
	}

	//var txrwsetv1 *commonv1.TxRWSet
	txrwsetv1 := &commonv1.TxRWSet{}

	err := proto.Unmarshal(rwsetBytes, txrwsetv1)
	if err != nil {
		return err
	}

	txwsetv1 := txrwsetv1.TxWrites

	for _, txWrite := range txwsetv1 {

		txWriteKeyMap[common.ConstructKey(txWrite.ContractName, txWrite.Key)] = &commonPb.TxWrite{
			Key:          txWrite.Key,
			Value:        txWrite.Value,
			ContractName: txWrite.ContractName,
		}
	}
	return nil
}
