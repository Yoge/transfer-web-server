package chainmaker_sdk

import (
	commonv1 "chainmaker.org/chainmaker-go/pb/protogo/common"
	"chainmaker.org/chainmaker/common/v2/evmutils"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"encoding/hex"
	"fmt"
	"github.com/gogo/protobuf/proto"
	"math/big"
	"regexp"
)

var (
	expr  = `^[0-9]{40,}$`
	re, _ = regexp.Compile(expr)
)

/*
	进行first block的构建我们需要将genesis block与chainmaker1.x高度为1的block合并构建到一个区块中。其中我们只需要将genesis tx,作为
	安装辅助合约交易的一个参数。
	chainmaker1.x block 1 存在两种情况
	1: chainmaker1.x block 1 is config block. 我们将config tx同样设为安装辅助合约交易的一个参数。
	2: chainmaker1.x block 1 is initcontract tx. 转为正常的chainmaker安装合约交易。
*/
//由chainmake1.x 生成chainmaker2.x的第一个区块
func (tb *TxBatchImpl) CreateCMFirstTxBatchSDK(bs []*commonv1.BlockInfo) ([]*common.Transaction, error) {

	//length := bs[1].Block.Header.TxCount

	txBatch := make([]*common.Transaction, 0, 0)

	//1. 生成添加短证书交易，使用public模式暂时不使用短证书
	if tb.authType == "permissionedwithcert" { //修改为小写
		tx1, err := tb.createShutCertTx()
		if err != nil {
			return nil, err
		}
		txBatch = append(txBatch, tx1)
	}

	//2. 生成创建迁移辅助合约交易
	kvs := make([]*common.KeyValuePair, 0, 1)
	genesisBytes, err := proto.Marshal(bs[0].Block.Txs[0])
	if err != nil {
		return nil, err
	}
	originTxId := "" //chainmaker 所有版本的创世区块，都一样所以这里不能和fanbric一样，使用创世区块txId
	genesiskv := &common.KeyValuePair{
		Key:   "genesis_tx",
		Value: genesisBytes,
	}
	kvs = append(kvs, genesiskv)
	req2, err := tb.createTransferContractTx(kvs, originTxId)
	if err != nil {
		return nil, err
	}
	txBatch = append(txBatch, req2)

	txs, err := tb.ParseBlocktoFirstTxbatchSDK(bs[1])
	if err != nil {
		return nil, err
	}
	txBatch = append(txBatch, txs...)

	//update
	err = tb.updateClient()
	if err != nil {
		return nil, err
	}

	tb.log.Debugf("Create chainmaker2.x block1.txBatch success")
	return txBatch, nil
}

//解析chainmaker1.x block1生成chainmaker2.x的txBatch
//1号区块特殊处理，因为这个时候迁移辅助合约还没有创建成功
func (tb *TxBatchImpl) ParseBlocktoFirstTxbatchSDK(b *commonv1.BlockInfo) ([]*common.Transaction, error) {

	txBatch := make([]*common.Transaction, 0, len(b.Block.Txs))

	for i, tx := range b.Block.Txs {
		//生成安装合约交易
		if tx.Header.TxType == commonv1.TxType_MANAGE_USER_CONTRACT && tx.Result.Code == commonv1.TxStatusCode_SUCCESS {
			//var payload = &commonv1.TransactPayload{}
			//如果是安装合约payload是这个&commonv1.ContractMgmtPayload{}结构体，不是&commonv1.TransactPayload{}
			var payload = &commonv1.ContractMgmtPayload{}
			err := payload.Unmarshal(tx.RequestPayload)
			if err != nil {
				return nil, err
			}

			//构建init 写入写集合，更新进来
			normalkvs := make([]*common.KeyValuePair, 0, 1)
			isEvm := payload.ContractId.RuntimeType == commonv1.RuntimeType_EVM
			txWSet, err := parseInitWSet(b.RwsetList[i], isEvm)
			if err != nil {
				return nil, err
			}
			if len(txWSet) > 0 {
				txRWSet := &commonv1.TxRWSet{
					TxId:     b.RwsetList[i].TxId,
					TxWrites: txWSet,
				}

				rwSetBytes, err := proto.Marshal(txRWSet)
				if err != nil {
					return nil, err
				}
				rwsetkv := &common.KeyValuePair{
					Key:   "origin_rwset",
					Value: rwSetBytes,
				}
				normalkvs = append(normalkvs, rwsetkv)
			}
			contract_name := payload.ContractId.ContractName
			if isEvm {
				address, err := evmutils.MakeAddressFromString(contract_name)
				if err != nil {
					return nil, err
				}
				contract_name = hex.EncodeToString(address.Bytes())
			}
			req2, err := tb.client.CreateContractCreate(payload.ContractId.ContractName, tx.Header.TxId, defaultVersion, tb.userContractPath,
				runtime, normalkvs, &common.Limit{GasLimit: 0}, tx.Header.Timestamp)
			if err != nil {
				return nil, err
			}
			tx2 := &common.Transaction{
				Payload:   req2.Payload,
				Sender:    req2.Sender,
				Endorsers: req2.Endorsers,
				Result:    nil,
			}

			txBatch = append(txBatch, tx2)
		} else {
			//Block1的非合约安装交易，简单存证一下即可（T合约N方法）
			specialkvs := make([]*common.KeyValuePair, 0, 1)

			txBytes, err := proto.Marshal(tx)
			if err != nil {
				return nil, err
			}

			specialkv := &common.KeyValuePair{
				Key:   "special_tx",
				Value: txBytes,
			}

			specialkvs = append(specialkvs, specialkv)

			req4, err := tb.client.InvokeContract(syscontract.SystemContract_T.String(), syscontract.TestContractFunction_N.String(), tx.Header.TxId,
				specialkvs, 0, false, &common.Limit{GasLimit: 0}, tx.Header.Timestamp)
			if err != nil {
				return nil, err
			}
			tx4 := &common.Transaction{
				Payload:   req4.Payload,
				Sender:    req4.Sender,
				Endorsers: req4.Endorsers,
				Result:    nil,
			}

			txBatch = append(txBatch, tx4)
		}
	}
	return txBatch, nil
}

// ParseBlocktoTxbatchSDK 解析chainmaker1.x block2、3...，生成chainmaker2.x的txBatch
func (tb *TxBatchImpl) ParseBlocktoTxbatchSDK(b *commonv1.BlockInfo) ([]*common.Transaction, error) {

	txBatch := make([]*common.Transaction, 0, len(b.Block.Txs))
	if b.Block.Header.BlockHeight == 2 || b.Block.Header.BlockHeight == 4 {
		tb.log.Debugf("===")
	}
	var req *common.TxRequest
	//合约中用来获取方法的
	methodkv := &common.KeyValuePair{
		Key:   "method",
		Value: []byte("transfer"),
	}
	for i, tx := range b.Block.Txs {
		if tx.Result.Code != commonv1.TxStatusCode_SUCCESS {
			normalkvs := make([]*common.KeyValuePair, 0, 1)
			txBytes0, err := proto.Marshal(tx)
			if err != nil {
				return nil, err
			}
			//normalkvs 增加 该笔tx的原始交易
			originkv := &common.KeyValuePair{
				Key:   "special_tx",
				Value: txBytes0,
			}
			normalkvs = append(normalkvs, originkv)

			normalkvs = append(normalkvs, methodkv)
			req, err = tb.client.InvokeContract(tb.assistContractName, invoke_contract, tx.Header.TxId,
				normalkvs, 0, false, &common.Limit{GasLimit: 0}, tx.Header.Timestamp)
			if err != nil {
				return nil, err
			}

			//生成安装合约交易，安装合约也存在当前安装合约的调用
		} else if tx.Header.TxType == commonv1.TxType_MANAGE_USER_CONTRACT {
			//如果是安装合约payload是这个&commonv1.ContractMgmtPayload{}结构体，不是&commonv1.TransactPayload{}
			var payload = &commonv1.ContractMgmtPayload{}
			err := payload.Unmarshal(tx.RequestPayload)
			if err != nil {
				return nil, err
			}

			//构建init 写入写集合，更新进来
			normalkvs := make([]*common.KeyValuePair, 0, 1)
			isEvm := payload.ContractId.RuntimeType == commonv1.RuntimeType_EVM
			txWSet, err := parseInitWSet(b.RwsetList[i], isEvm)
			if err != nil {
				return nil, err
			}
			if len(txWSet) > 0 {
				txRWSet := &commonv1.TxRWSet{
					TxId:     b.RwsetList[i].TxId,
					TxWrites: txWSet,
				}

				rwSetBytes, err := proto.Marshal(txRWSet)
				if err != nil {
					return nil, err
				}
				rwsetkv := &common.KeyValuePair{
					Key:   "origin_rwset",
					Value: rwSetBytes,
				}
				normalkvs = append(normalkvs, rwsetkv)
			}
			if "INIT_CONTRACT" != payload.Method { //升级合约或者是冻结等合约修改为迁移辅助合约调用
				txBytes0, err := proto.Marshal(tx)
				if err != nil {
					return nil, err
				}
				//normalkvs 增加 该笔tx的原始交易
				originkv := &common.KeyValuePair{
					Key:   "origin_tx",
					Value: txBytes0,
				}
				normalkvs = append(normalkvs, originkv)

				normalkvs = append(normalkvs, methodkv)
				req, err = tb.client.InvokeContract(tb.assistContractName, invoke_contract, tx.Header.TxId,
					normalkvs, 0, false, &common.Limit{GasLimit: 0}, tx.Header.Timestamp)
				if err != nil {
					return nil, err
				}
			} else {
				contract_name := payload.ContractId.ContractName
				if isEvm {
					address, err := evmutils.MakeAddressFromString(contract_name)
					if err != nil {
						return nil, err
					}
					contract_name = hex.EncodeToString(address.Bytes())
				}
				req, err = tb.client.CreateContractCreate(contract_name, tx.Header.TxId, defaultVersion, tb.userContractPath,
					runtime, normalkvs, &common.Limit{GasLimit: 0}, tx.Header.Timestamp)
				if err != nil {
					return nil, err
				}
			}

			//如果是正常的交易
		} else if tx.Header.TxType == commonv1.TxType_INVOKE_USER_CONTRACT {
			normalkvs := make([]*common.KeyValuePair, 0, 1)

			txBytes0, err := proto.Marshal(tx)
			if err != nil {
				return nil, err
			}

			normalkvs = append(normalkvs, methodkv)

			//normalkvs 增加 该笔tx的原始交易
			originkv := &common.KeyValuePair{
				Key:   "origin_tx",
				Value: txBytes0,
			}
			normalkvs = append(normalkvs, originkv)

			//normalkvs 增加 该笔tx对应的txrwset。
			rwset := b.RwsetList[i]
			//只需要写集重新上链即可，读集没有上链的意义，去掉
			rwset.TxReads = make([]*commonv1.TxRead, 0)
			//if payload.ContractId.RuntimeType == commonv1.RuntimeType_EVM{
			txWSet, err := updateEvmContractName(rwset.TxWrites)
			if err != nil {
				return nil, err
			}
			rwset.TxWrites = txWSet
			//	}

			rwSetBytes, err := proto.Marshal(rwset)
			if err != nil {
				return nil, err
			}
			rwsetkv := &common.KeyValuePair{
				Key:   "origin_rwset",
				Value: rwSetBytes,
			}
			normalkvs = append(normalkvs, rwsetkv)

			req, err = tb.client.InvokeContract(tb.assistContractName, invoke_contract, tx.Header.TxId,
				normalkvs, 0, false, &common.Limit{GasLimit: 0}, tx.Header.Timestamp)
			if err != nil {
				return nil, err
			}

		} else {
			//其他的非合约调用，非安装合约的交易，就不产生读写集
			specialkvs := make([]*common.KeyValuePair, 0, 1)

			txBytes, err := proto.Marshal(tx)
			if err != nil {
				return nil, err
			}

			specialkvs = append(specialkvs, methodkv)

			//之前交易的原始内容
			specialkv := &common.KeyValuePair{
				Key:   "special_tx",
				Value: txBytes,
			}
			specialkvs = append(specialkvs, specialkv)

			req, err = tb.client.InvokeContract(tb.assistContractName, invoke_contract, tx.Header.TxId,
				specialkvs, 0, false, &common.Limit{GasLimit: 0}, tx.Header.Timestamp)
			if err != nil {
				return nil, err
			}

		}
		tx := &common.Transaction{
			Payload:   req.Payload,
			Sender:    req.Sender,
			Endorsers: req.Endorsers,
			Result:    nil,
		}
		txBatch = append(txBatch, tx)
	}
	return txBatch, nil
}

//当合约在安装和更新时，会调用init or upgrade 方法，也会产生当前合约的读写集合，所以我们需要将读写集合提取出来。
func parseInitWSet(txRWSet *commonv1.TxRWSet, isEvm bool) ([]*commonv1.TxWrite, error) {
	txWSet := make([]*commonv1.TxWrite, 0)

	for _, wSet := range txRWSet.TxWrites {
		if wSet.ContractName != commonv1.ContractName_SYSTEM_CONTRACT_STATE.String() {
			txWSet = append(txWSet, wSet)
		}
	}

	if isEvm {
		return updateEvmContractName(txWSet)
	}
	return txWSet, nil
}

//需要增加判断条件，检查是否是evm contractName
func updateEvmContractName(txWSet []*commonv1.TxWrite) ([]*commonv1.TxWrite, error) {

	for _, set := range txWSet {
		if !re.MatchString(set.ContractName) {
			continue
		}
		addInt, ok := big.NewInt(0).SetString(set.ContractName, 10)
		if !ok {
			return nil, fmt.Errorf("chainmaker1.x evm contratc name parse fail")
		}
		set.ContractName = hex.EncodeToString(addInt.Bytes())
	}

	return txWSet, nil
}
