/**
 * @Author: starxxliu
 * @Date: 2021/11/16 10:18 上午
 */

package chainmaker_sdk

import (
	"chainmaker.org/chainmaker/transfer-tool/provider"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"regexp"
	"time"

	commonv1 "chainmaker.org/chainmaker-go/pb/protogo/common"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/protocol/v2"
	commonl "chainmaker.org/chainmaker/transfer-tool/common"
	chainMaker "chainmaker.org/chainmaker/transfer-tool/common/chainmaker-sdk"
	"chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-tool/db/sqlite"
	"chainmaker.org/chainmaker/transfer-tool/fabric/fabric_1.4/fabric"
	"chainmaker.org/chainmaker/transfer-tool/utils"
	"github.com/hyperledger/fabric-protos-go/peer"
	"go.uber.org/zap"
)

const (
	lscc        = "lscc"
	lsccInstall = "install"
	lsccDeploy  = "deploy"
	lsccUpgrade = "upgrade"
	escc        = "escc"
	vscc        = "vscc"
	SysLscc     = "lscc"
)

var (
	startHeight     = 0 //记录我们迁移的起始高度
	defaultVersion  = "v1.0.0"
	runtime         = common.RuntimeType_DOCKER_GO
	limit           = 10
	invoke_contract = "invoke_contract"
)

type TxBatchImpl struct {
	contractName       map[string]uint64 //key is contractName value is install block height
	client             *chainMaker.ChainClient
	log                *zap.SugaredLogger
	assistContractName string
	transferMethod     string
	userContractPath   string
	assistContractPath string
	authType           string
	db                 provider.TransferDB
}

func NewTxBatchImpl(config *config.Config, certHash []byte, log *zap.SugaredLogger, db provider.TransferDB) (*TxBatchImpl, error) {
	permissioned := chainMaker.StringToAuthTypeMap[config.Chain.AuthType]

	enableShortCert := false
	if len(certHash) > 0 {
		enableShortCert = true
	}
	client, err := chainMaker.NewChainClient(
		chainMaker.WithChainClientOrgId(config.Chain.OrgId),
		chainMaker.WithChainClientChainId(config.Chain.ChainId),
		chainMaker.WithChainClientLogger(log),
		chainMaker.WithUserSignKeyFilePath(config.Chain.UserSignKeyFilePath),
		chainMaker.WithUserSingCrtFilePath(config.Chain.UserSignCrtFilePath),

		chainMaker.WithHashType(config.Chain.HashType),
		chainMaker.WithAuthType(permissioned),
		//支持短证书交易
		chainMaker.WithEnabledCrtHash(enableShortCert),
		chainMaker.WithUserCrtHash(certHash),
	)
	if err != nil {
		return nil, err
	}

	if enableShortCert && len(client.GetClientUserCrtHash()) > 0 {
		commonl.ShortCerts[hex.EncodeToString(certHash)] = client.GetClientUserCrtBytes()
	}

	tx := &TxBatchImpl{
		//contractName:       conn,
		client:             client,
		log:                log,
		assistContractPath: config.ContractInfo.AssistContractPath,
		userContractPath:   config.ContractInfo.UserContractPath,
		assistContractName: config.ContractInfo.AssistContractName,
		transferMethod:     config.ContractInfo.TransferMethod,
		authType:           config.Chain.AuthType,
		db:                 db,
	}
	return tx, nil
}

//CreateTxBatch create chainmaker tx for fabric block
func (tb *TxBatchImpl) CreateTxBatch(bInfo *commonv1.BlockInfo) ([]*common.Transaction, error) {
	return tb.ParseBlocktoTxbatchSDK(bInfo)
}

/*
	进行first block的构建我们需要将genesis block与fabric高度为1的block合并构建到一个区块中。其中我们只需要将fabric genesis tx,作为
	安装辅助合约交易的一个参数。
	fabric block 1 存在两种情况
	1: fabric block 1 is config block. 我们将config tx同样设为安装辅助合约交易的一个参数。
	2: fabric block 1 is lscc tx.(其中只会是deploy合约交易). 转为正常的chainmaker安装合约交易。
*/
func (tb *TxBatchImpl) CreateFirstTxBatch(bs []*commonv1.BlockInfo) ([]*common.Transaction, error) {
	return tb.CreateCMFirstTxBatchSDK(bs)

}

func (tb *TxBatchImpl) GetUserCrtByte() []byte {
	return tb.client.GetClientUserCrtBytes()
}

func (tb *TxBatchImpl) GetClientUserCrtHash() []byte {
	return tb.client.GetClientUserCrtHash()
}

//updateClient 2.2.0 version use pk
func (tb *TxBatchImpl) updateClient() error {
	if tb.client.GetClientAuth() == chainMaker.Public {
		return nil
	}
	err := tb.client.UpdateCertHash(true, nil)
	if err != nil {
		return err
	}

	//tb.db.UpdateUserCrtHash(string(tb.client.GetClientUserCrtBytes()),"")
	//db := sqlite.GetDB()
	//status, err := sqlite.GetStatus(db, "1")
	//if err != nil {
	//	return err
	//}
	//
	//status.EnableShortCert = true
	//status.CertHash = string(tb.client.GetClientUserCrtBytes())

	//err = status.UpdateChain(db)
	//if err != nil {
	//	return err
	//}
	return nil
}

func (tb *TxBatchImpl) createShutCertTx() (*common.Transaction, error) {
	req, err := tb.client.AddCert()
	if err != nil {
		return nil, err
	}
	tx := &common.Transaction{
		Payload:   req.Payload,
		Sender:    req.Sender,
		Endorsers: req.Endorsers,
		Result:    nil}
	return tx, nil
}

func (tb *TxBatchImpl) createTransferContractTx(kvs []*common.KeyValuePair, txId string) (*common.Transaction, error) {
	req, err := tb.client.CreateContractCreate(tb.assistContractName, txId, defaultVersion, tb.assistContractPath,
		runtime, kvs, nil, time.Now().Unix())
	if err != nil {
		return nil, err
	}

	tx := &common.Transaction{
		Payload:   req.Payload,
		Sender:    req.Sender,
		Endorsers: req.Endorsers,
		Result:    nil}
	return tx, nil
}

func (tb *TxBatchImpl) filterContractName(name string) bool {
	if name == SysLscc {
		return true
	}
	return false
}

func (tb *TxBatchImpl) filterSysContractName(name string) bool {
	if name == SysLscc {
		return true
	}
	return false
}

func (tb *TxBatchImpl) createKvs(env *fabric.Envelope, originTx, method []byte, isConfig, isValid,
	isContract bool, code peer.TxValidationCode) ([]*common.KeyValuePair, error) {
	is_valid := "valid"
	if !isValid {
		is_valid = "invalid"
	}
	is_config := "normal"
	if isConfig {
		is_config = "config"
	}
	kvs := []*common.KeyValuePair{
		{
			Key:   "origin_tx",
			Value: originTx,
		},
		{
			Key:   "is_valid",
			Value: []byte(is_valid),
		},
		{
			Key:   "tx_type",
			Value: []byte(is_config),
		},
	}

	if code == peer.TxValidationCode_DUPLICATE_TXID {
		kvs = append(kvs, &common.KeyValuePair{
			Key:   "origin_txid",
			Value: []byte(env.Payload.Header.ChannelHeader.TxId),
		},
		)
	}
	if method != nil {
		kvs = append(kvs, &common.KeyValuePair{
			Key:   "method",
			Value: method,
		},
		)
	}
	var tx_wset []byte
	var err error
	if isContract { //表示当前交易属于实例化合约或者是升级合约交易，提取读写集
		if !isValid || isConfig {
			tx_wset = nil
		} else {
			wSet := make(map[string][]byte, 0)
			for _, v := range env.Payload.Transaction.ChaincodeAction.Response.TxRwSet.NsRwSets {
				if tb.filterSysContractName(v.NameSpace) {
					continue
				}
				for _, wSet := range v.KvRwSet.Writes {
					wSet.Key, err = utils.FixKey(wSet.Key)
					if err != nil {
						return nil, err
					}
					err = checkKey(wSet.Key)
					if err != nil {
						return nil, err
					}
				}

				wsetByte, err := json.Marshal(v.KvRwSet.Writes)
				if err != nil {
					return nil, err
				}

				wSet[v.NameSpace] = wsetByte
			}

			tx_wset, err = json.Marshal(wSet)
			if err != nil {
				return nil, err
			}
			kvNC := []*common.KeyValuePair{
				{
					Key:   "tx_wset",
					Value: tx_wset,
				},
			}

			kvs = append(kvs, kvNC...)
		}
	}
	return kvs, nil
}

func checkParameters(reqBatch []*common.Transaction) error {
	for _, req := range reqBatch {
		for _, p := range req.Payload.Parameters {
			match, err := regexp.MatchString(protocol.DefaultStateRegex, p.Key)
			if err != nil || !match {
				return fmt.Errorf(
					"expect key no special characters, but got key:[%s]. letter, number, dot and underline are allowed",
					p.Key)
			}
		}
	}
	return nil
}

func checkKey(key string) error {
	match, err := regexp.MatchString(protocol.DefaultStateRegex, key)
	if err != nil || !match {
		return fmt.Errorf(
			"expect key no special characters, but got key:[%s]. letter, number, dot and underline are allowed",
			key)
	}
	return nil
}

func (tb *TxBatchImpl) insertContract(name string, height uint64) error {
	db := sqlite.GetDB()
	contract := sqlite.Contract{
		ContractName: name,
		Height:       height,
	}
	_, err := contract.InsertContract(db)
	if err != nil {
		return err
	}

	return nil
}
