/**
 * @Author: starxxliu
 * @Date: 2021/12/9 10:46 上午
 */

package chainMaker

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

//CreateContract create contract and return new contract path
func CreateContract(contractDir, sourceFile, destinationFile string) (string, error) {
	sourcePath := filepath.Join(contractDir, sourceFile)
	destinationPath := filepath.Join(contractDir, destinationFile)
	compressPath := filepath.Join(contractDir, destinationFile+".7z")

	if _, err := os.Stat(compressPath); !os.IsNotExist(err) {
		return compressPath, nil
	}

	err := copyFile(sourcePath, destinationPath)
	if err != nil {
		return "", err
	}
	err = compressFile(destinationPath)
	if err != nil {
		return "", err
	}

	return compressPath, nil
}

func copyFile(sourceFile, destinationFile string) error {
	if sourceFile == destinationFile {
		return nil
	}

	input, err := ioutil.ReadFile(sourceFile)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(destinationFile, input, 0644)
	if err != nil {
		return err
	}
	return nil
}

//压缩文件并删除源文件
func compressFile(zipPath string) error {

	copyCommand := fmt.Sprintf("7z a %s.7z %s", zipPath, zipPath) // contract1

	err := runCmd(copyCommand)
	if err == nil {
		return os.Remove(zipPath)
	}

	return err
}

// RunCmd exec cmd
func runCmd(command string) error {
	commands := strings.Split(command, " ")
	cmd := exec.Command(commands[0], commands[1:]...) // #nosec

	if err := cmd.Start(); err != nil {
		return err
	}

	return cmd.Wait()
}
