/**
 * @Author: starxxliu
 * @Date: 2021/11/17 11:52 上午
 */

package chainMaker

import (
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"strings"
	"time"

	"chainmaker.org/chainmaker/common/v2/crypto"
	bcx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"
	"chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/sdk-go/v2/utils"
	"chainmaker.org/chainmaker/transfer-tool/config"
	utilsL "chainmaker.org/chainmaker/transfer-tool/utils"
	"go.uber.org/zap"
)

type AuthType uint32

const (
	defaultSeq = 0

	// permissioned with certificate
	PermissionedWithCert AuthType = iota + 1

	// permissioned with public key
	PermissionedWithKey

	// public key
	Public
)

var AuthTypeToStringMap = map[AuthType]string{
	PermissionedWithCert: "permissionedwithcert",
	PermissionedWithKey:  "permissionedwithkey",
	Public:               "public",
}

var StringToAuthTypeMap = map[string]AuthType{
	"permissionedwithcert": PermissionedWithCert,
	"permissionedwithkey":  PermissionedWithKey,
	"public":               Public,
}

type ChainClient struct {
	// common config
	logger *zap.SugaredLogger
	//pool         ConnectionPool

	userSignKeyFilePath string // 公钥模式下使用该字段
	userSignCrtFilePath string
	userSignKeyBytes    []byte // 公钥模式下使用该字段
	userSignCrtBytes    []byte
	// 以下字段为经过处理后的参数
	privateKey crypto.PrivateKey // 证书和公钥身份模式都使用该字段存储私钥

	chainId      string
	orgId        string
	userCrtBytes []byte
	userCrt      *bcx509.Certificate

	// 公钥模式下
	userPk crypto.PublicKey
	crypto *CryptoConfig

	// cert hash config
	enabledCrtHash bool
	userCrtHash    []byte

	publicKey crypto.PublicKey
	pkBytes   []byte
	hashType  string
	authType  AuthType
}

type CryptoConfig struct {
	hash string
}

func NewChainClient(opts ...ChainClientOption) (*ChainClient, error) {
	config, err := generateConfig(opts...)
	if err != nil {
		return nil, err
	}

	var hashType = ""
	var publicKey crypto.PublicKey
	var pkBytes []byte
	var pkPem string

	if config.authType == PermissionedWithKey || config.authType == Public {
		hashType = config.hashType
		publicKey = config.userPk
		pkPem, err = publicKey.String()
		if err != nil {
			return nil, err
		}

		pkBytes = []byte(pkPem)
	}

	return &ChainClient{
		logger:         config.logger,
		chainId:        config.chainId,
		orgId:          config.orgId,
		userCrtBytes:   config.userCrtBytes,
		userCrt:        config.userCrt,
		privateKey:     config.privateKey,
		enabledCrtHash: config.enabledCrtHash,
		userCrtHash:    config.userCrtHash,
		publicKey:      publicKey,
		pkBytes:        pkBytes,
		hashType:       hashType,
		authType:       config.authType,
	}, nil
}

func (cc *ChainClient) GetClientAuth() AuthType {
	return cc.authType
}

func (cc *ChainClient) GetClientUserCrtBytes() []byte {
	return cc.userCrtBytes
}

func (cc *ChainClient) GetClientUserCrtHash() []byte {
	return cc.userCrtHash
}

func (cc *ChainClient) CreateContractCreate(contractName, txId, version, byteCodeStringOrFilePath string,
	runtime common.RuntimeType, kvs []*common.KeyValuePair, limit *common.Limit, timestamp int64) (*common.TxRequest, error) {

	payload, err := cc.createContractManageWithByteCodePayload(contractName,
		syscontract.ContractManageFunction_INIT_CONTRACT.String(), version, txId, byteCodeStringOrFilePath, runtime, kvs, limit)
	if err != nil {
		return nil, err
	}
	payload.Timestamp = timestamp

	endorsers, err := cc.GetEndorsersWithAuthType(crypto.HashAlgoMap[cc.hashType], payload)
	if err != nil {
		return nil, err
	}

	return cc.generateTxRequest(payload, endorsers)

}

func (cc *ChainClient) InvokeContract(contractName, method, txId string, kvs []*common.KeyValuePair, timeout int64,
	withSyncResult bool, limit *common.Limit, timestamp int64) (*common.TxRequest, error) {
	payload := cc.createPayload(txId, common.TxType_INVOKE_CONTRACT, contractName, method, kvs, defaultSeq, limit)
	payload.Timestamp = timestamp

	return cc.generateTxRequest(payload, nil)
}

func (cc *ChainClient) AddCert() (*common.TxRequest, error) {
	cc.logger.Infof("[SDK] begin to add cert, [contract:%s]/[method:%s]",
		syscontract.SystemContract_CERT_MANAGE.String(), syscontract.CertManageFunction_CERT_ADD.String())
	payload := cc.createPayload("", common.TxType_INVOKE_CONTRACT, syscontract.SystemContract_CERT_MANAGE.String(),
		syscontract.CertManageFunction_CERT_ADD.String(), nil, defaultSeq, nil)

	return cc.generateTxRequest(payload, nil)

}

func (cc *ChainClient) createPayload(txId string, txType common.TxType, contractName, method string,
	kvs []*common.KeyValuePair, seq uint64, limit *common.Limit) *common.Payload {
	if txId == "" {
		txId = utils.GetRandTxId()
	}

	payload := utils.NewPayload(
		utils.WithChainId(cc.chainId),
		utils.WithTxType(txType),
		utils.WithTxId(txId),
		utils.WithTimestamp(time.Now().Unix()),
		utils.WithContractName(contractName),
		utils.WithMethod(method),
		utils.WithParameters(kvs),
		utils.WithSequence(seq),
		utils.WithLimit(limit),
		//limit
	)

	return payload
}

func (cc *ChainClient) createContractManagePayload(contractName, method string, limit *common.Limit) (*common.Payload, error) {
	kvs := []*common.KeyValuePair{
		{
			Key:   syscontract.GetContractInfo_CONTRACT_NAME.String(),
			Value: []byte(contractName),
		},
	}
	return cc.createPayload("", common.TxType_INVOKE_CONTRACT, syscontract.SystemContract_CONTRACT_MANAGE.String(),
		method, kvs, defaultSeq, limit), nil
}

func (cc *ChainClient) createContractManageWithByteCodePayload(contractName, method, version, txId string,
	byteCodeStringOrFilePath string, runtime common.RuntimeType, kvs []*common.KeyValuePair, limit *common.Limit) (*common.Payload, error) {
	var (
		codeBytes []byte
	)

	isFile := utils.Exists(byteCodeStringOrFilePath)
	if isFile {
		path, file := utilsL.SplitPath(byteCodeStringOrFilePath)
		byteCodeStringOrFilePath, err := CreateContract(path, file, contractName)
		if err != nil {
			return nil, fmt.Errorf("create contract %s failed, %s", byteCodeStringOrFilePath, err)
		}

		bz, err := ioutil.ReadFile(byteCodeStringOrFilePath)
		if err != nil {
			return nil, fmt.Errorf("read from byteCode file %s failed, %s", byteCodeStringOrFilePath, err)
		}

		if runtime == common.RuntimeType_EVM { // evm contract hex need decode to bytes
			codeBytesStr := strings.TrimSpace(string(bz))
			if codeBytes, err = hex.DecodeString(codeBytesStr); err != nil {
				return nil, fmt.Errorf("decode evm contract hex to bytes failed, %s", err)
			}
		} else { // wasm bin file no need decode
			codeBytes = bz
		}
	} else {
		return nil, fmt.Errorf("can not get contract filepath ")
	}

	if !cc.checkKeyValuePair(kvs) {
		return nil, fmt.Errorf("use reserved word")
	}

	payload := cc.createPayload(txId, common.TxType_INVOKE_CONTRACT,
		syscontract.SystemContract_CONTRACT_MANAGE.String(), method, kvs, defaultSeq, limit)

	payload.Parameters = append(payload.Parameters, &common.KeyValuePair{
		Key:   syscontract.InitContract_CONTRACT_NAME.String(),
		Value: []byte(contractName),
	})

	payload.Parameters = append(payload.Parameters, &common.KeyValuePair{
		Key:   syscontract.InitContract_CONTRACT_VERSION.String(),
		Value: []byte(version),
	})

	payload.Parameters = append(payload.Parameters, &common.KeyValuePair{
		Key:   syscontract.InitContract_CONTRACT_RUNTIME_TYPE.String(),
		Value: []byte(runtime.String()),
	})

	payload.Parameters = append(payload.Parameters, &common.KeyValuePair{
		Key:   syscontract.InitContract_CONTRACT_BYTECODE.String(),
		Value: codeBytes,
	})

	return payload, nil
}

func (cc *ChainClient) checkKeyValuePair(kvs []*common.KeyValuePair) bool {
	for _, kv := range kvs {
		if kv.Key == syscontract.InitContract_CONTRACT_NAME.String() ||
			kv.Key == syscontract.InitContract_CONTRACT_RUNTIME_TYPE.String() ||
			kv.Key == syscontract.InitContract_CONTRACT_VERSION.String() ||
			kv.Key == syscontract.InitContract_CONTRACT_BYTECODE.String() ||
			kv.Key == syscontract.UpgradeContract_CONTRACT_NAME.String() ||
			kv.Key == syscontract.UpgradeContract_CONTRACT_RUNTIME_TYPE.String() ||
			kv.Key == syscontract.UpgradeContract_CONTRACT_VERSION.String() ||
			kv.Key == syscontract.UpgradeContract_CONTRACT_BYTECODE.String() {
			return false
		}
	}

	return true
}

func (cc *ChainClient) generateTxRequest(payload *common.Payload,
	endorsers []*common.EndorsementEntry) (*common.TxRequest, error) {
	var (
		signer    *accesscontrol.Member
		signBytes []byte
		err       error
	)

	// 构造Sender
	if cc.authType == PermissionedWithCert {

		if cc.enabledCrtHash && len(cc.userCrtHash) > 0 {
			signer = &accesscontrol.Member{
				OrgId:      cc.orgId,
				MemberInfo: cc.userCrtHash,
				MemberType: accesscontrol.MemberType_CERT_HASH,
			}
		} else {
			signer = &accesscontrol.Member{
				OrgId:      cc.orgId,
				MemberInfo: cc.userCrtBytes,
				MemberType: accesscontrol.MemberType_CERT,
			}
		}
	} else {
		signer = &accesscontrol.Member{
			OrgId:      cc.orgId,
			MemberInfo: cc.pkBytes,
			MemberType: accesscontrol.MemberType_PUBLIC_KEY,
		}
	}

	req := &common.TxRequest{
		Payload: payload,
		Sender: &common.EndorsementEntry{
			Signer:    signer,
			Signature: nil,
		},
		Endorsers: endorsers,
	}

	if cc.authType == PermissionedWithCert {
		hashalgo, err := bcx509.GetHashFromSignatureAlgorithm(cc.userCrt.SignatureAlgorithm)
		if err != nil {
			return nil, fmt.Errorf("invalid algorithm: %v", err.Error())
		}

		signBytes, err = utils.SignPayloadWithHashType(cc.privateKey, hashalgo, payload)
		if err != nil {
			return nil, fmt.Errorf("SignPayload failed, %s", err.Error())
		}
	} else {
		signBytes, err = utils.SignPayloadWithHashType(cc.privateKey, crypto.HashAlgoMap[cc.hashType], payload)
		if err != nil {
			return nil, fmt.Errorf("SignPayload failed, %s", err.Error())
		}
	}

	req.Sender.Signature = signBytes

	return req, nil
}

func (cc *ChainClient) UpdateCertHash(enabledCrtHash bool, crtHash []byte) (err error) {
	if crtHash == nil {
		crtHash, err = utils.GetCertificateId(cc.userCrtBytes, config.TransferConfig.Chain.HashType)
		if err != nil {
			return err
		}
	}
	cc.userCrtHash = crtHash
	cc.enabledCrtHash = enabledCrtHash
	//cc.authType = PermissionedWithKey
	return nil
}
