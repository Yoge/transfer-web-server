/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

// Package chainconf record all the values of the chain config options.
package chainconf

import (
	"errors"
	"fmt"

	"chainmaker.org/chainmaker/pb-go/v2/config"

	"io/ioutil"
	"path/filepath"

	"chainmaker.org/chainmaker/common/v2/json"

	//cchainconf "chainmaker.org/chainmaker/chainconf/v2"
	"github.com/spf13/viper"
)

const (
	AllContract = "ALL_CONTRACT"

	blockEmptyErrorTemplate = "block is empty"
)

var blockEmptyError = errors.New(blockEmptyErrorTemplate)

// Genesis will create new genesis config block of chain.
func Genesis(genesisFile, provider string) (*config.ChainConfig, error) {
	chainConfig := &config.ChainConfig{Contract: &config.ContractConfig{EnableSqlSupport: false}}
	fileInfo := map[string]interface{}{}
	v := viper.New()
	v.SetConfigFile(genesisFile)
	if err := v.ReadInConfig(); err != nil {
		return nil, err
	}
	if err := v.Unmarshal(&fileInfo); err != nil {
		return nil, err
	}
	bytes, err := json.Marshal(fileInfo)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(bytes, chainConfig)
	if err != nil {
		return nil, err
	}

	//if v.Get("core.enable_conflicts_bit_window") == nil {
	//	chainConfig.Core.EnableConflictsBitWindow = true
	//}

	// load the trust root certs than set the bytes as value
	// need verify org and root certs
	for _, root := range chainConfig.TrustRoots {
		for i := 0; i < len(root.Root); i++ {
			filePath := root.Root[i]
			if !filepath.IsAbs(filePath) {
				filePath, err = filepath.Abs(filePath)
				if err != nil {
					return nil, err
				}
			}
			entry, err := ioutil.ReadFile(filePath)
			if err != nil {
				return nil, fmt.Errorf("fail to read whiltlist file [%s]: %v", filePath, err)
			}
			root.Root[i] = string(entry)
		}
	}

	// load the trust member certs than set the bytes as value
	// need verify org
	trustMemberInfoMap := make(map[string]bool, len(chainConfig.TrustMembers))
	for _, member := range chainConfig.TrustMembers {
		filePath := member.MemberInfo
		if !filepath.IsAbs(filePath) {
			filePath, err = filepath.Abs(filePath)
			if err != nil {
				return nil, err
			}
		}
		entry, err := ioutil.ReadFile(filePath)
		if err != nil {
			return nil, fmt.Errorf("fail to read trust memberInfo file [%s]: %v", filePath, err)
		}
		if _, ok := trustMemberInfoMap[string(entry)]; ok {
			return nil, fmt.Errorf("the trust member info is exist, member info: %s", string(entry))
		}
		member.MemberInfo = string(entry)
		trustMemberInfoMap[string(entry)] = true
	}

	// verify
	_, err = VerifyChainConfig(chainConfig, provider)
	if err != nil {
		return nil, err
	}

	return chainConfig, nil
}

func GetChainConfig(genesisFile, provider string) (*config.ChainConfig, error) {
	chainConfig := &config.ChainConfig{Contract: &config.ContractConfig{EnableSqlSupport: false}}
	fileInfo := map[string]interface{}{}
	v := viper.New()
	v.SetConfigFile(genesisFile)
	if err := v.ReadInConfig(); err != nil {
		return nil, err
	}
	if err := v.Unmarshal(&fileInfo); err != nil {
		return nil, err
	}
	bytes, err := json.Marshal(fileInfo)
	if err != nil {
		return nil, err
	}
	err = json.Unmarshal(bytes, chainConfig)
	if err != nil {
		return nil, err
	}

	return chainConfig, nil
}
