/**
 * @Author: starxxliu
 * @Date: 2021/11/10 8:08 下午
 */

package simulation

import (
	"context"
	"fmt"

	"chainmaker.org/chainmaker/transfer-tool/provider"

	consensuspb "chainmaker.org/chainmaker/pb-go/v2/consensus"
	"chainmaker.org/chainmaker/transfer-tool/common"
	"chainmaker.org/chainmaker/transfer-tool/common/msgbus"
	"chainmaker.org/chainmaker/transfer-tool/config"
	tbftblock "chainmaker.org/chainmaker/transfer-tool/core/simulation/consensus"
	"chainmaker.org/chainmaker/transfer-tool/core/simulation/core/syncmode/proposer"
	"chainmaker.org/chainmaker/transfer-tool/loggers"
	"go.uber.org/zap"
)

type CoreEngine struct {
	msgBus            msgbus.MessageBus // message bus, transfer messages with other modules
	msgBusC           msgbus.MessageBus // message bus, transfer messages with other modules
	Proposer          *proposer.BlockProposerImpl
	LatestBlockHeight uint64
	preHash           []byte
	logger            *zap.SugaredLogger
	err               chan error
	signChan          chan *provider.TempBlock
}

// NewCoreEngine new a core engine.
func NewCoreEngine(height uint64, pre []byte, errChan chan error, bus, busc msgbus.MessageBus, sim provider.SimContractExec) (*CoreEngine, error) {
	logger := loggers.GetLogger(loggers.MODULE_PARSER)

	chain := config.TransferConfig.Chain
	//node := config.TransferConfig.Node
	nodeId := chain.ConsensusId

	identity, err := common.GetIdentity(chain.AuthType, chain.HashType, chain.OrgId, chain.PrivKeyFilePath, "", chain.CertFilePath)
	if err != nil {
		return nil, err
	}

	signChan := make(chan *provider.TempBlock, tbftblock.DelFaultChanCap)
	proposer := proposer.NewBlockProposerImpl(chain.ChainId, nodeId, chain.HashType, identity, logger, busc, signChan, sim)
	proposer.SetPreHash(pre)

	core := &CoreEngine{
		msgBus:            bus,
		msgBusC:           busc,
		logger:            logger,
		preHash:           pre,
		LatestBlockHeight: height,
		Proposer:          proposer,
		err:               errChan,
		signChan:          signChan,
	}
	return core, nil
}

// OnQuit called when quit subsribe message from message bus
func (c *CoreEngine) OnQuit() {
	//c.log.Info("on quit")
}

// OnMessage consume a message from message bus
func (c *CoreEngine) OnMessage(message *msgbus.Message) {
	switch message.Topic {
	case msgbus.BuildProposal:
		//TODO 进行proposing invoke message.payload istxRequestBatch
		block, ok := message.Payload.(*provider.TempBlock)
		if !ok {
			err := fmt.Errorf("not expect block type. expect provider TempBlock")
			c.logger.Error(err)
			c.Proposer.SendMes(err, false)
		}
		if block.Done {
			if block.Err != nil {
				c.logger.Error(block.Err)
			}
			c.Proposer.SendMes(block.Err, true)
			//c.msgBus.Close()
			return
		}
		c.logger.Debugf("core engine consume a batch tx,tx num[%d]", len(block.TxBatch))
		c.LatestBlockHeight++
		if block.Height != c.LatestBlockHeight {
			if block.Err != nil {
				c.Proposer.SendMes(block.Err, false)
			} else {
				err := fmt.Errorf("block height must is [%d],but is [%d]", c.LatestBlockHeight, block.Height)
				c.logger.Error(err)
				c.Proposer.SendMes(err, false)
			}
			//c.msgBus.Close()
			return
		}

		var err error
		if block.Proposal == nil {
			_, block.Proposal, err = c.Proposer.Proposing(c.LatestBlockHeight, c.preHash, block)
			if err != nil {
				c.logger.Error(err)
				c.Proposer.SendMes(err, false)
				return
			}
		}

		c.signChan <- block

	}
}

func (c *CoreEngine) PreFirstBlock(block *provider.TempBlock) (*consensuspb.ProposalBlock, error) {
	c.LatestBlockHeight++
	preHash, proposal, err := c.Proposer.Proposing(c.LatestBlockHeight, c.preHash, block)
	if err != nil {
		return nil, err
	}

	finalBlock, err := c.Proposer.HandleProposedBlock(proposal)
	if err != nil {
		return nil, err
	}
	proposal.Block = finalBlock

	preHash = finalBlock.Hash()
	c.preHash = preHash
	c.Proposer.SetPreHash(preHash)

	return proposal, nil
}

// Start initialize core engine
func (c *CoreEngine) Start(ctx context.Context) {

	c.msgBus.Register(msgbus.BuildProposal, c) // 我们使用这个topic 来代表TxRequestBatch
	c.Proposer.StartConsensus(ctx)
}
