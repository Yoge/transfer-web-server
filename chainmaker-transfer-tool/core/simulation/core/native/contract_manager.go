/**
 * @Author: starxxliu
 * @Date: 2021/11/18 9:10 下午
 */

package native

import (
	"encoding/hex"
	"encoding/json"
	"errors"

	"chainmaker.org/chainmaker/pb-go/v2/accesscontrol"

	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	configPb "chainmaker.org/chainmaker/pb-go/v2/config"
	"chainmaker.org/chainmaker/pb-go/v2/syscontract"
	"chainmaker.org/chainmaker/transfer-tool/utils"
	"github.com/gogo/protobuf/proto"
	"github.com/hyperledger/fabric-protos-go/ledger/rwset/kvrwset"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/core/ledger/kvledger/txmgmt/rwsetutil"
)

var (
	InstallContractName               = syscontract.SystemContract_CONTRACT_MANAGE.String()
	paramWSetName                     = "tx_wset"
	OriginTx                          = "origin_tx"
	IsValid                           = "is_valid"
	InValid                           = "invalid"
	SystemContract_CONTRACT_MANAGE    = "CONTRACT_MANAGE"
	SystemContract_CERT_MANAGE        = "CERT_MANAGE"
	keyContractName                   = "_Native_Contract_List"
	SystemContract_CHAIN_CONFIG       = "CHAIN_CONFIG"
	SystemContract_CHAIN_CONFIG_Value = []byte{} //configPb.ChainConfig marshal DATA
	FullCertMemberInfo                = []byte{}
)

func SimInstallContract(tx *commonPb.Transaction, name, version string, byteCode []byte,
	runTime commonPb.RuntimeType, initParameters map[string][]byte) (map[string]*commonPb.TxWrite, []byte, error) {
	if !utils.CheckContractNameFormat(name) {
		return nil, nil, errors.New("invalid contract name")
	}
	txWriteKeyMap := make(map[string]*commonPb.TxWrite, 0) //后期会进行相应的排序

	key := utils.GetContractDbKey(name)

	signer := tx.Sender.GetSigner()
	creator := &accesscontrol.MemberFull{
		OrgId:      signer.OrgId,
		MemberType: signer.MemberType,
		MemberInfo: signer.MemberInfo,
	}

	contract := &commonPb.Contract{
		Name:        name,
		Version:     version,
		RuntimeType: runTime,
		Status:      commonPb.ContractStatus_NORMAL,
		Creator:     creator,
	}
	cdata, _ := contract.Marshal()

	txWriteKeyMap[ConstructKey(InstallContractName, key)] = &commonPb.TxWrite{
		Key:          key,
		Value:        cdata,
		ContractName: InstallContractName,
	}

	byteCodeKey := utils.GetContractByteCodeDbKey(name)
	txWriteKeyMap[ConstructKey(InstallContractName, byteCodeKey)] = &commonPb.TxWrite{
		Key:          byteCodeKey,
		Value:        byteCode,
		ContractName: InstallContractName,
	}

	err := CreateWriteKeyMap(txWriteKeyMap, initParameters)
	if err != nil {
		return nil, nil, err
	}

	contractByte, _ := contract.Marshal()
	return txWriteKeyMap, contractByte, nil
}

func CreateWriteKeyMap(txWriteKeyMap map[string]*commonPb.TxWrite, wSet map[string][]byte) error {
	res := wSet[paramWSetName]
	if res == nil {
		return nil
	}

	fWSet := make(map[string][]byte, 0)
	err := json.Unmarshal(res, &fWSet)
	if err != nil {
		return err
	}

	for k, writes := range fWSet {
		kvWSet := make([]*kvrwset.KVWrite, 0)
		err := json.Unmarshal(writes, &kvWSet)
		if err != nil {
			return err
		}
		for _, kv := range kvWSet {
			value := kv.Value
			if kv.IsDelete {
				value = nil
			}

			txWriteKeyMap[ConstructKey(k, []byte(kv.Key))] = &commonPb.TxWrite{
				Key:          []byte(kv.Key),
				Value:        value,
				ContractName: k,
			}
		}
	}

	return nil
}

func CreateTxWriteKeyMap(txWriteKeyMap map[string]*commonPb.TxWrite, wSet map[string][]byte, txRwSet *rwsetutil.TxRwSet) error {
	res := wSet[OriginTx]
	if res == nil {
		return nil
	}

	var err error
	if txRwSet != nil {
		txRwSet, err = utils.ParseFabricTx(res)
		if err != nil {
			return err
		}
	}

	if txRwSet == nil { //如果是config 交易txRwSet为nil
		return nil
	}

	if string(wSet[IsValid]) == InValid {
		return nil
	}

	fWSet, err := utils.GetWSet(txRwSet)
	if err != nil {
		return err
	}

	for k, writes := range fWSet {
		//kvWSet := make([]*kvrwset.KVWrite, 0)
		//err := json.Unmarshal(writes, &kvWSet)
		//if err != nil {
		//	return err
		//}
		for _, kv := range writes {
			value := kv.Value
			if kv.IsDelete {
				value = nil
			}

			txWriteKeyMap[ConstructKey(k, []byte(kv.Key))] = &commonPb.TxWrite{
				Key:          []byte(kv.Key),
				Value:        value,
				ContractName: k,
			}
		}
	}

	return nil
}

//FetchDisabledContractList fetch the disabled native contract list
func FetchDisabledContractList(first bool) (map[string]*commonPb.TxRead, map[string]*commonPb.TxWrite, error) {
	//create _Native_Contract_List readSet  如果存在这里是固定的
	txReadKeyMap := make(map[string]*commonPb.TxRead, 0)
	txWriteKeyMap := make(map[string]*commonPb.TxWrite, 0) //后期会进行相应的排序

	if first {
		txReadKeyMap[ConstructKey(SystemContract_CONTRACT_MANAGE, []byte(keyContractName))] = &commonPb.TxRead{
			Key:          []byte(keyContractName),
			Value:        nil,
			ContractName: SystemContract_CONTRACT_MANAGE,
			Version:      nil,
		}
	} else {
		disabledContractListBytes, _ := json.Marshal(nil)
		txReadKeyMap[ConstructKey(SystemContract_CONTRACT_MANAGE, []byte(keyContractName))] = &commonPb.TxRead{
			Key:          []byte(keyContractName),
			Value:        disabledContractListBytes,
			ContractName: SystemContract_CONTRACT_MANAGE,
			Version:      nil,
		}
	}

	var err error
	//first block CHAIN_CONFIG readSet 这里是chainConfig configPb.ChainConfig
	if first {
		txReadKeyMap[ConstructKey(SystemContract_CHAIN_CONFIG, []byte(SystemContract_CHAIN_CONFIG))] = &commonPb.TxRead{
			Key:          []byte(SystemContract_CHAIN_CONFIG),
			Value:        SystemContract_CHAIN_CONFIG_Value,
			ContractName: SystemContract_CHAIN_CONFIG,
			Version:      nil,
		}

		cc := configPb.ChainConfig{}
		err = proto.Unmarshal(SystemContract_CHAIN_CONFIG_Value, &cc)
		if err != nil {
			return nil, nil, err
		}

		disabledContractListBytes, err := json.Marshal(cc.DisabledNativeContract)
		if err != nil {
			return nil, nil, err
		}

		txWriteKeyMap, err = CreateDisabledContractList(disabledContractListBytes)
		if err != nil {
			return nil, nil, err
		}
	}

	return txReadKeyMap, txWriteKeyMap, nil
}

// CreateDisabledContractList store the disabled contract list to the database
func CreateDisabledContractList(refinedDisabledContractList []byte) (map[string]*commonPb.TxWrite, error) {
	txWriteKeyMap := make(map[string]*commonPb.TxWrite, 0)

	txWriteKeyMap[ConstructKey(SystemContract_CONTRACT_MANAGE, []byte(keyContractName))] = &commonPb.TxWrite{
		Key:          []byte(keyContractName),
		Value:        refinedDisabledContractList, //"null"
		ContractName: SystemContract_CONTRACT_MANAGE,
	}

	return txWriteKeyMap, nil
}

func CreateSenderReader(memberInfo []byte, fullCertMemberInfo []byte) map[string]*commonPb.TxRead {
	txReadKeyMap := make(map[string]*commonPb.TxRead, 0)
	memberInfoHex := hex.EncodeToString(memberInfo)
	if fullCertMemberInfo == nil {
		fullCertMemberInfo = FullCertMemberInfo
	}
	txReadKeyMap[ConstructKey(SystemContract_CERT_MANAGE, []byte(memberInfoHex))] = &commonPb.TxRead{
		Key:          []byte(memberInfoHex),
		Value:        fullCertMemberInfo,
		ContractName: SystemContract_CERT_MANAGE,
		Version:      nil,
	}
	return txReadKeyMap
}

func CreateQueryContractReader(name string) map[string]*commonPb.TxRead {
	txReadKeyMap := make(map[string]*commonPb.TxRead, 0)
	key := utils.GetContractDbKey(name)

	txReadKeyMap[ConstructKey(SystemContract_CONTRACT_MANAGE, key)] = &commonPb.TxRead{
		Key:          key,
		Value:        nil,
		ContractName: SystemContract_CONTRACT_MANAGE,
		Version:      nil,
	}
	return txReadKeyMap
}

func InstallContractReader(memberInfo []byte, fullCertMemberInfo []byte, name string, isCrtHash bool) (map[string]*commonPb.TxRead, map[string]*commonPb.TxWrite, error) {
	txReadKeyMap := make(map[string]*commonPb.TxRead, 0)
	//txWriteKeyMap := make(map[string]*commonPb.TxWrite, 0) //后期会进行相应的排序

	//txRS, txWs, err := FetchDisabledContractList(false) //2.2.0 version del
	//if err != nil {
	//	return nil, nil, err
	//}

	//if isCrtHash {
	//	txRSS := CreateSenderReader(memberInfo, fullCertMemberInfo)
	//	for key, read := range txRSS {
	//		txReadKeyMap[key] = read
	//	}
	//}
	txRSC := CreateQueryContractReader(name)
	for key, read := range txRSC {
		txReadKeyMap[key] = read
	}
	return txReadKeyMap, nil, nil
}
