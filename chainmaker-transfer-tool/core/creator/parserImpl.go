/**
 * @Author: starxxliu
 * @Date: 2021/11/16 9:16 下午
 */

package creator

import (
	"context"
	"fmt"
	"time"

	"chainmaker.org/chainmaker/transfer-tool/common/msgbus"
	"chainmaker.org/chainmaker/transfer-tool/config"
	"chainmaker.org/chainmaker/transfer-tool/core/simulation/core/syncmode/proposer"
	"chainmaker.org/chainmaker/transfer-tool/loggers"
	"chainmaker.org/chainmaker/transfer-tool/provider"
	"github.com/panjf2000/ants/v2"
	"go.uber.org/zap"
)

var (
	defaultBlocks = 548
)

type TxCreator struct {
	txBatch provider.ChainMakerTxBatch //update support mul chain

	//fetcher *fabric.Fetcher  //update to interface

	msgBus msgbus.MessageBus // message bus, transfer messages with other modules

	logger *zap.SugaredLogger

	send chan *provider.SendInfo

	currentHeight uint64

	Proposer *proposer.BlockProposerImpl

	cacheMap map[uint64]*provider.TempBlock

	tempBlockChan chan *provider.TempBlock //update interface param
	//thresholdValue做为消费者的阀值
	thresholdValue uint64

	//add parse interface
	creator provider.Fetcher
}

//NewParserImpl new TxCreator
func NewParserImpl2(bus msgbus.MessageBus, db provider.TransferDB, proposer *proposer.BlockProposerImpl,
	tbatch provider.ChainMakerTxBatch, creator provider.Fetcher, send chan *provider.SendInfo, taskId string) (*TxCreator, error) {
	logger := loggers.GetLogger(loggers.MODULE_PARSER)

	saveHeight, err := db.GetSaveHeight(taskId)
	if err != nil {
		return nil, err
	}

	return &TxCreator{
		msgBus:         bus,
		txBatch:        tbatch,
		logger:         logger,
		send:           send,
		currentHeight:  saveHeight + 1,
		Proposer:       proposer,
		cacheMap:       make(map[uint64]*provider.TempBlock),
		tempBlockChan:  make(chan *provider.TempBlock, defaultBlocks),
		thresholdValue: uint64(creator.GetThresholdRound()) + saveHeight + 1,
		creator:        creator,
	}, nil
}

func (tc *TxCreator) GetTxCreatorCrtByte() []byte {
	return tc.txBatch.GetUserCrtByte()
}

//Start start TxCreator
func (tc *TxCreator) Start(ctx context.Context) error {
	err := tc.creator.Start(ctx)
	if err != nil {
		return err
	}

	go tc.loop(tc.send, ctx, config.TransferConfig.Async.ParseCapable)
	return nil
}

//PreFirstBlock create first block TxCreator
func (tc *TxCreator) PreFirstBlock() (*provider.TempBlock, error) {
	blocks := make([]interface{}, 2)
	//把源区块链的0号和1号区块塞入到新的1号区块中
	for i := 0; i < 2; i++ {
		block, err := tc.creator.QueryBlockByHeight(uint64(i))
		if err != nil {
			return nil, err
		}
		blocks[i] = block

	}
	tc.currentHeight++

	tc.creator.UpdateState()

	txs, err := tc.txBatch.CreateFirstTxBatch(blocks)
	if err != nil {
		tc.logger.Error(err)
		return nil, err
	}

	tb := &provider.TempBlock{
		Height: 0,
		//OBlock:  nil,
		OBlock:  blocks[1],
		TxBatch: txs,
		Done:    false,
		Err:     nil,
	}

	return tb, nil
}

func (tc *TxCreator) GetFetcher() provider.Fetcher {
	return tc.creator
}

func (tc *TxCreator) GetTxBatch() provider.ChainMakerTxBatch {
	return tc.txBatch
}

func (tc *TxCreator) GetProposer() *proposer.BlockProposerImpl {
	return tc.Proposer
}

// Close fetcher close
func (tc *TxCreator) Close(err error) {
	tc.logger.Error(err)
	tb := &provider.TempBlock{
		Err: err,
	}
	tc.tempBlockChan <- tb
}

// OnQuit called when quit subsribe message from message bus
func (tc *TxCreator) OnQuit() {
	//tc.logger.Info("on quit")
}

// OnMessage consume a message from message bus
func (tc *TxCreator) OnMessage(message *msgbus.Message) {
	//nill realize
}

func (tc *TxCreator) loop(send chan *provider.SendInfo, ctx context.Context, poolCapacity int) {
	tc.logger.Debugf("start loop txcreator module,start height[%d]", tc.currentHeight)
	goRoutinePool, err := ants.NewPool(poolCapacity, ants.WithPreAlloc(true))
	if err != nil {
		panic("tx creator mod fail,err " + err.Error())
	}
	defer goRoutinePool.Release()

	for {
		select {
		case <-ctx.Done():
			tc.logger.Infof("close chainmaker txRequest creator goroutine %s", ctx.Err())
			//tc.Close()
			return

		case bInfo := <-send:
			goRoutinePool.Submit(func() {
				startT := time.Now()
				b, err := tc.creator.ParseBlock(bInfo.Block) //反序列化fabric block
				if err != nil {
					tc.Close(fmt.Errorf("parse origin chain[%d] block fail,err[%+v]", bInfo.Height, err))
					return
				}
				pend := time.Now()
				tc.logger.Debugf("parse block[%d] end,spend time[%d]ns", bInfo.Height,
					pend.Sub(startT).Nanoseconds())

				txs, err := tc.txBatch.CreateTxBatch(b)
				if err != nil {
					tc.Close(fmt.Errorf("fabric[%d] block create txBatch fail,err[%+v]", bInfo.Height, err))
					return
				}
				tc.logger.Debugf("create block[%d] txRequests end,spend time[%+v],parse block,spend time[%d]us", bInfo.Height,
					time.Since(pend), pend.Sub(startT).Microseconds())
				tb := &provider.TempBlock{
					Height: bInfo.Height,
					//FBlock:  b,
					OBlock:  b,
					TxBatch: txs,
				}
				//增加sim schedule 部分
				if tc.Proposer != nil {
					_, tb.Proposal, err = tc.Proposer.Proposing(bInfo.Height, nil, tb)
					if err != nil {
						tc.Close(fmt.Errorf("origin chain[%d] block proposing fail,err[%+v]", bInfo.Height, err))
						return
					}
				}
				tc.tempBlockChan <- tb
			})

		case tb := <-tc.tempBlockChan:
			if tc.handleBlock(tb) { //已经处理完全部的迁移数据，或者是迁移过程中遇到了err
				return
			}
		}
	}
}

func (tc *TxCreator) handleBlock(tb *provider.TempBlock) bool {
	tc.logger.Debugf("get temp txBatch,fabric block height[%d]", tb.Height)
	if tb.Err != nil {
		tc.msgBus.PublishSafe(msgbus.BuildProposal, tb)
		return true
	}
	if tb.Height != tc.currentHeight {
		tc.cacheMap[tb.Height] = tb

	} else {
		tc.logger.Debugf("PublishSafe temp txBatch,fabric block height[%d]", tb.Height)

		tc.msgBus.PublishSafe(msgbus.BuildProposal, tb)
		tc.currentHeight++

		for {
			if tc.thresholdValue == tc.currentHeight-1 {
				tc.creator.NextRound() //update threshold value
				tc.thresholdValue += uint64(tc.creator.GetRoundNum())
			}

			tb, ok := tc.cacheMap[tc.currentHeight]
			if !ok {
				break
			}
			tc.logger.Debugf("PublishSafe temp txBatch,fabric block height[%d]", tb.Height)

			tc.msgBus.PublishSafe(msgbus.BuildProposal, tb)
			delete(tc.cacheMap, tc.currentHeight)

			tc.currentHeight++
		}

		if tc.creator.IsFinish(tc.currentHeight - 1) { //已经处理完全部的迁移数据
			tc.logger.Infof("has been processed latest fabric block")
			tc.creator.Close()
			tb := &provider.TempBlock{
				Done: true,
				Err:  nil,
			}
			tc.msgBus.PublishSafe(msgbus.BuildProposal, tb)
			return true
		}
		//tc.logger.Debugf("cache map cap[%d]",len(tc.cacheMap))
	}
	return false
}
