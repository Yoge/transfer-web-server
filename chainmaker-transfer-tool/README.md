## 整体架构

### 迁移辅助合约assist
提供transfer方法，接收参数包括：
1. special_tx，不做任何处理。
2. origin_rwset，跨合约调用业务合约，生成写集。
3. origin_tx，与origin_rwset成对出现，合约内不做任何处理。
### 迁移转换合约transfer
该合约与源链上的合约名一致，但是处理逻辑很简单，就是接收参数tx_wset，然后产生写集。不关心方法名。

##区块的处理过程
### 目标区块视角
#### 0号区块
创世区块，根据bc1.yml创建
#### 1号区块
1. 如果是证书模式，这Tx0是创建短证书的交易，如果不是，则忽略这一步。
2. 然后是创建一个迁移辅助合约的交易。这个交易的TxId是源链的Block0.Tx0的TxID。而且输入参数genesis_tx是源链的创世交易。
3. 然后是源链Block1中的交易对应的Tx，这里出来两种情况，如果是安装合约交易，那么也创建安装合约交易，而且合约名相同，只不过内容是迁移合约。如果是其他类型的交易，则只是上链一个原始交易即可。
### 2号和后面的区块
与源区块高度和交易一一对应，而且分3种情况：
1. 安装合约交易，则对应安装用户合约。origin_rwset参数保留写集
2. 用户合约调用交易，对应合约调用交易，而且写集相同，origin_tx和origin_rwset作为参数。
3. 其他交易，原始交易信息上链即可，参数origin_tx。