/**
 * @Author: p_starxxliu
 * @Date: 2021/4/20 4:09 下午
 */
package loggers

import (
	"chainmaker.org/chainmaker/transfer-web-server/config"
	"testing"
)

func TestLogger(t *testing.T) {
	confPath := "../../config"
	env := "dev"
	config.InitConfig(confPath, env)

	SetLogConfig(&config.KaiwuConfig.LogConfig)

	logger := GetLogger(MODULE_USER)
	logger.Info("alarm info log ......")
	logger.Warn("alarm warn log ......")
	logger.Error("alarm error log ......")
}

